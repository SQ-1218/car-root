package test;

import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.mongodb.DB;
import com.mongodb.Mongo;

@RunWith(SpringJUnit4ClassRunner.class)  //使用junit4进行测试      
@ContextConfiguration(locations={"classpath*:mongodb.xml"})    
public class MongodbConnectionTest {

	@Test  
    public void connection(){  
        try{       
            // 连接到 mongodb 服务    
             Mongo mongo = new Mongo("192.168.1.88", 27017);      
            //根据mongodb数据库的名称获取mongodb对象 ,    
             DB db = mongo.getDB( "database" );   
             
             
             Set<String> collectionNames = db.getCollectionNames();      
               // 打印出test中的集合      
              for (String name : collectionNames) {      
                    System.out.println("collectionName==="+name);      
              }      
                 
          }catch(Exception e){    
             e.printStackTrace();    
          }    
    }  
	  
}
