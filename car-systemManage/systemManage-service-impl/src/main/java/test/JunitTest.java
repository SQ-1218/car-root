package test;

import java.math.BigDecimal;

import org.apache.shiro.codec.Base64;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magicrule.prj.common.pinyin.ChineseCharToPy;

public class JunitTest {

	protected static Logger log = LoggerFactory.getLogger(JunitTest.class);
	
	public void test1() {
		Double resultValue=13.567;
		BigDecimal bd=new BigDecimal(resultValue);
		BigDecimal bd2=bd.setScale(2, BigDecimal.ROUND_HALF_UP);
		Double get_double=bd2.doubleValue();
		 System.out.println(  get_double );
		 System.out.println( Base64.encodeToString("magicRule2018".getBytes()) );
		 System.out.println( Base64.decodeToString(Base64.encode("magicRule2018".getBytes())) );
		 System.out.println( Base64.decodeToString("bWFnaWNSdWxlMjAxOA==") );
	}
	
	@Test
	public void test2() {
		ChineseCharToPy py=new ChineseCharToPy();
		log.info(py.getAllFirstLetter("测试汉字"));
		log.info(py.getFirstLetter("测试汉字"));
		log.info("cshz".substring(0, 3));
	}
}
