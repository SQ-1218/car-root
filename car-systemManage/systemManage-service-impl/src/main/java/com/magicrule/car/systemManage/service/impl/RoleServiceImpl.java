package com.magicrule.car.systemManage.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.magicrule.car.systemManage.dao.api.RoleDaoApi;
import com.magicrule.car.systemManage.model.Role;
import com.magicrule.car.systemManage.service.api.RoleServiceApi;
import com.magicrule.prj.common.ConfigConsts;

/**
 * 
 * @ClassName:  RoleServiceImpl   
 * @Description:TODO(角色Service实现类)   
 * @author: hebb
 * @date:   2019年2月11日 下午9:33:56   
 *     
 * @Copyright: 2019 www.magicruler.com Inc. All rights reserved. 
 * 注意：本内容仅限于魔尺信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目的
 */
@Transactional
@Service
public class RoleServiceImpl implements RoleServiceApi{

	protected static Logger log = LoggerFactory.getLogger(RoleServiceImpl.class);  
	
	@Autowired
	RoleDaoApi roleDaoApi;
	
	/**
	 * 
	 * @Title: removeRole   
	 * @Description: TODO(删除角色)   
	 * @param: @param roleList
	 * @param: @return      
	 * @return: int      
	 * @throws
	 */
	public String removeRole(List<Role> roleList){
		List<Role> resultList=getRoleLisByMultiId(roleList);
		for(Role role:resultList) {
			//判断是否存在可用的权限
			if("Y".equals(role.getEnabled())) {
				return ConfigConsts.EXIST_ENABLED;
			}
		}
		
		roleDaoApi.removeRole(roleList);
		
		return ConfigConsts.SUCCESS;
		
	}
	
	/**
	 * 
	 * @Title: updateRole   
	 * @Description: TODO(修改角色)   
	 * @param: @param role
	 * @param: @return      
	 * @return: int      
	 * @throws
	 */
	public int updateRole(Role role){
		return roleDaoApi.updateRole(role);
	}
	
	/**
	 * 
	 * @Title: insertRole   
	 * @Description: TODO(新增角色)   
	 * @param: @param role
	 * @param: @return      
	 * @return: int      
	 * @throws
	 */
	public int insertRole(Role role){
		return roleDaoApi.insertRole(role);
	}

	/**
	 * 
	 * @Title: findRoleList   
	 * @Description: TODO(分页查询角色列表)   
	 * @param: @param role
	 * @param: @return      
	 * @return: List<Role>      
	 * @throws
	 */
	public List<Role> findRoleList(Role role){
		return roleDaoApi.findRoleList(role);
	}
	
	/**
	 * 
	 * @Title: getRoleById   
	 * @Description: TODO(根据角色Id查询角色)   
	 * @param: @param id
	 * @param: @return      
	 * @return: Role      
	 * @throws
	 */
	public Role getRoleById(String id) {
		return roleDaoApi.getRoleById(id);
	}

	/**
	 * 
	 * @Title: getRoleLisByMultiId   
	 * @Description: TODO(根据多个id查询权限)    
	 * @param roleList
	 * @return
	 */
	@Override
	public List<Role> getRoleLisByMultiId(List<Role> roleList) {
		return roleDaoApi.getRoleLisByMultiId(roleList);
	}
	
	
	
}
