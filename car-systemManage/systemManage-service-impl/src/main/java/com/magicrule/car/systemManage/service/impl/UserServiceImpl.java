package com.magicrule.car.systemManage.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.magicrule.car.systemManage.dao.api.UserDaoApi;
import com.magicrule.car.systemManage.model.Role;
import com.magicrule.car.systemManage.model.RoleResourcePermission;
import com.magicrule.car.systemManage.model.User;
import com.magicrule.car.systemManage.service.api.UserServiceApi;
import com.magicrule.prj.common.ConfigConsts;

/**
 * 
 * @ClassName:  UserServiceImpl   
 * @Description:TODO(用户Service实现类)   
 * @author: hebb
 * @date:   2018年11月28日 下午10:27:25   
 *     
 * @Copyright: 2018 www.magicruler.com Inc. All rights reserved. 
 * 注意：本内容仅限于魔尺信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目的
 */
@Transactional
@Service
public class UserServiceImpl implements UserServiceApi{

	protected static Logger log = LoggerFactory.getLogger(UserServiceImpl.class);  
	
	@Autowired
	UserDaoApi userDaoApi;
	
	/**
	 * 
	 * @Title: removeUser   
	 * @Description: TODO(删除用户)   
	 * @param: @param userList
	 * @param: @return      
	 * @return: int      
	 * @throws
	 */
	public String removeUser(List<User> userList) {
		List<User> resultList=getUserLisByMultiId(userList);
		for(User user:resultList) {
			//判断是否存在可用的权限
			if("Y".equals(user.getEnabled())) {
				return ConfigConsts.EXIST_ENABLED;
			}
		}
		
		userDaoApi.removeUser(userList);
		
		return ConfigConsts.SUCCESS;
	}
	
	
	/**
	 * 
	 * @Title: updateUser   
	 * @Description: TODO(修改用户)    
	 * @param user
	 * @return
	 */
	public int updateUser(User user) {
		return userDaoApi.updateUser(user);
	}

	/**
	 * 
	 * @Title: insertUser   
	 * @Description: TODO(新增用户)    
	 * @param user
	 * @return
	 */
	public int insertUser(User user) {
		return userDaoApi.insertUser(user);
	}

	/**
	 * 
	 * @Title: findUserList   
	 * @Description: TODO(分页查询用户列表)    
	 * @param user
	 * @return
	 */
	@Transactional(readOnly = true)
	public List<User> findUserList(User user) {
		return userDaoApi.findUserList(user);
	}

	/**
	 * 
	 * @Title: getByUserName   
	 * @Description: TODO(根据用户名称查找用户)   
	 * @param: @param user
	 * @param: @return      
	 * @return: User      
	 * @throws
	 */
	@Transactional(readOnly = true)
	public User getUserByUserName(String userName) {
		return userDaoApi.getUserByUserName(userName);
	}
	
	/**
	 * 
	 * @Title: getUserById   
	 * @Description: TODO(根据用户Id查找用户)   
	 * @param: @param userName
	 * @param: @return      
	 * @return: User      
	 * @throws
	 */
	@Transactional(readOnly = true)
	public User getUserById(Long id) {
		return userDaoApi.getUserById(id);
	}
	
	/**
	 * 
	 * @Title: getRolesByUserName   
	 * @Description: TODO(根据用户名称查找该用户的角色)   
	 * @param: @param userId
	 * @param: @return      
	 * @return: List<String>      
	 * @throws
	 */
	@Transactional(readOnly = true)
	public List<Role> getRoleListByUserId(Long userId) {
		return userDaoApi.getRoleListByUserId(userId);
	}
	
	/**
	 * 
	 * @Title: getPermissionsByRole   
	 * @Description: TODO(根据角色名称查找角色相关的资源)   
	 * @param: @param roleList
	 * @param: @return      
	 * @return: List<RoleResourcePermission>      
	 * @throws
	 */
	@Transactional(readOnly = true)
	public List<RoleResourcePermission> getPermissionsByRoleList(List<Role> roleList){
		return userDaoApi.getPermissionsByRoleList(roleList);
	}


	/**
	 * 
	 * @Title: getUserLisByMultiId   
	 * @Description: TODO(根据多个id查询权限)   
	 * @param: @param userList
	 * @param: @return      
	 * @return: List<User>      
	 * @throws
	 */
	@Override
	public List<User> getUserLisByMultiId(List<User> userList) {
		return userDaoApi.getUserLisByMultiId(userList);
	}



	
}
