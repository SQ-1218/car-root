package com.magicrule.car.systemManage.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.magicrule.car.systemManage.dao.api.ResourceDaoApi;
import com.magicrule.car.systemManage.model.Resource;
import com.magicrule.car.systemManage.service.api.ResourceServiceApi;
import com.magicrule.prj.common.ConfigConsts;

/**
 * 
 * @ClassName:  ResourceServiceImpl   
 * @Description:TODO(菜单资源Service实现类)   
 * @author: hebb
 * @date:   2019年2月22日 下午10:38:51   
 *     
 * @Copyright: 2019 www.magicruler.com Inc. All rights reserved. 
 * 注意：本内容仅限于魔尺信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目的
 */
@Transactional
@Service
public class ResourceServiceImpl implements ResourceServiceApi{
	

	protected static Logger log = LoggerFactory.getLogger(RoleServiceImpl.class);  
	
	@Autowired
	ResourceDaoApi resourceDaoApi;

	/**
	 * 
	 * @Title: removeResource   
	 * @Description: TODO(删除菜单资源)    
	 * @param resourceList
	 * @return
	 */
	@Override
	public String removeResource(List<Resource> resourceList) {
		List<Resource> resultList=getResourceLisByMultiId(resourceList);
		for(Resource resource:resultList) {
			//判断是否存在可用的权限
			if("Y".equals(resource.getEnabled())) {
				return ConfigConsts.EXIST_ENABLED;
			}
		}
		
		resourceDaoApi.removeResource(resourceList);
		
		return ConfigConsts.SUCCESS;
	}

	/**
	 * 
	 * @Title: updateResource   
	 * @Description: TODO(修改菜单资源)    
	 * @param resource
	 * @return
	 */
	@Override
	public int updateResource(Resource resource) {
		return resourceDaoApi.updateResource(resource);
	}

	/**
	 * 
	 * @Title: insertResource   
	 * @Description: TODO(新增菜单资源)    
	 * @param resource
	 * @return
	 */
	@Override
	public int insertResource(Resource resource) {
		return resourceDaoApi.insertResource(resource);
	}

	/**
	 * 
	 * @Title: findResourceList   
	 * @Description: TODO(分页查询菜单资源列表)    
	 * @param resource
	 * @return
	 */
	@Override
	public List<Resource> findResourceList(Resource resource) {
		return resourceDaoApi.findResourceList(resource);
	}

	/**
	 * 
	 * @Title: getResourceById   
	 * @Description: TODO(根据Id查询菜单资源)    
	 * @param id
	 * @return
	 */
	@Override
	public Resource getResourceById(Long id) {
		return resourceDaoApi.getResourceById(id);
	}

	/**
	 * 
	 * @Title: getResourceLisByMultiId   
	 * @Description: TODO(根据多个id查询菜单资源)    
	 * @param resourceList
	 * @return
	 */
	@Override
	public List<Resource> getResourceLisByMultiId(List<Resource> resourceList) {
		return resourceDaoApi.getResourceLisByMultiId(resourceList);
	}

	/**
	 * 
	 * @Title: getResourceCountByCode   
	 * @Description: TODO(检查菜单编码是否已经被使用)    
	 * @param code
	 * @return
	 */
	@Override
	public Integer getResourceCountByCode(String code) {
		return resourceDaoApi.getResourceCountByCode(code);
	}

	
	
}
