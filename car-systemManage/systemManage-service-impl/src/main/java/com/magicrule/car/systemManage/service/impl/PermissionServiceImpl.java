package com.magicrule.car.systemManage.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.magicrule.car.systemManage.dao.api.PermissionDaoApi;
import com.magicrule.car.systemManage.model.Permission;
import com.magicrule.car.systemManage.service.api.PermissionServiceApi;
import com.magicrule.prj.common.ConfigConsts;

/**
 * 
 * @ClassName:  PermissionServiceImpl   
 * @Description:TODO(权限Service实现类)   
 * @author: hebb
 * @date:   2019年2月23日 下午5:40:18   
 *     
 * @Copyright: 2019 www.magicruler.com Inc. All rights reserved. 
 * 注意：本内容仅限于魔尺信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目的
 */
@Transactional
@Service
public class PermissionServiceImpl implements PermissionServiceApi{

	protected static Logger log = LoggerFactory.getLogger(PermissionServiceImpl.class);  
	
	@Autowired
	PermissionDaoApi permissionDaoApi;

	/**
	 * 
	 * @Title: removePermission   
	 * @Description: TODO(删除权限)    
	 * @param permissionList
	 * @return
	 */
	@Override
	public String removePermission(List<Permission> permissionList) {
		List<Permission> resultList=getPermissionLisByMultiId(permissionList);
		for(Permission permission:resultList) {
			//判断是否存在可用的权限
			if("Y".equals(permission.getEnabled())) {
				return ConfigConsts.EXIST_ENABLED;
			}
		}
		
		permissionDaoApi.removePermission(permissionList);
		
		return ConfigConsts.SUCCESS;
	}

	/**
	 * 
	 * @Title: updatePermission   
	 * @Description: TODO(修改权限)    
	 * @param permission
	 * @return
	 */
	@Override
	public int updatePermission(Permission permission) {
		return permissionDaoApi.updatePermission(permission);
	}

	/**
	 * 
	 * @Title: insertPermission   
	 * @Description: TODO(新增权限)    
	 * @param permission
	 * @return
	 */
	@Override
	public int insertPermission(Permission permission) {
		return permissionDaoApi.insertPermission(permission);
	}

	/**
	 * 
	 * @Title: findPermissionList   
	 * @Description: TODO(分页查询权限列表)    
	 * @param permission
	 * @return
	 */
	@Override
	public List<Permission> findPermissionList(Permission permission) {
		return permissionDaoApi.findPermissionList(permission);
	}

	/**
	 * 
	 * @Title: getPermissionById   
	 * @Description: TODO(根据Id查询权限)    
	 * @param id
	 * @return
	 */
	@Override
	public Permission getPermissionById(String id) {
		return permissionDaoApi.getPermissionById(id);
	}

	/**
	 * 
	 * @Title: getPermissionLisByMultiId   
	 * @Description: TODO(根据多个id查询权限)   
	 * @param: @param permissionList
	 * @param: @return      
	 * @return: List<Permission>      
	 * @throws
	 */
	@Override
	public List<Permission> getPermissionLisByMultiId(List<Permission> permissionList) {
		return permissionDaoApi.getPermissionLisByMultiId(permissionList);
	}
	
	
	
	
}
