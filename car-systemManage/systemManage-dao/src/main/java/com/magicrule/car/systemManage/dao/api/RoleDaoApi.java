package com.magicrule.car.systemManage.dao.api;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.magicrule.car.systemManage.model.Role;

/**
 * 
 * @ClassName:  RoleDaoApi   
 * @Description:TODO(角色Dao接口,insert/remove/update/get/find)   
 * @author: hebb
 * @date:   2019年1月30日 下午10:04:04   
 *     
 * @Copyright: 2019 www.magicruler.com Inc. All rights reserved. 
 * 注意：本内容仅限于魔尺信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目的
 */
public interface RoleDaoApi {

	/**
	 * 
	 * @Title: removeRole   
	 * @Description: TODO(删除角色)   
	 * @param: @param roleList
	 * @param: @return      
	 * @return: int      
	 * @throws
	 */
	int removeRole(@Param("roleList") List<Role> roleList);
	
	/**
	 * 
	 * @Title: updateRole   
	 * @Description: TODO(修改角色)   
	 * @param: @param role
	 * @param: @return      
	 * @return: int      
	 * @throws
	 */
	int updateRole(Role role);
	
	/**
	 * 
	 * @Title: insertRole   
	 * @Description: TODO(新增角色)   
	 * @param: @param role
	 * @param: @return      
	 * @return: int      
	 * @throws
	 */
	int insertRole(Role role);

	/**
	 * 
	 * @Title: findRoleList   
	 * @Description: TODO(分页查询角色列表)   
	 * @param: @param role
	 * @param: @return      
	 * @return: List<Role>      
	 * @throws
	 */
	List<Role> findRoleList(Role role);
	
	
	/**
	 * 
	 * @Title: getRoleById   
	 * @Description: TODO(根据角色Id查询角色)   
	 * @param: @param id
	 * @param: @return      
	 * @return: Role      
	 * @throws
	 */
	Role getRoleById(String id);
	
	/**
	 * 
	 * @Title: getRoleLisByMultiId   
	 * @Description: TODO(根据多个id查询权限)   
	 * @param: @param resourceList
	 * @param: @return      
	 * @return: List<Role>      
	 * @throws
	 */
	List<Role> getRoleLisByMultiId(@Param("roleList") List<Role> roleList);
	
	
}
