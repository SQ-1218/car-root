package com.magicrule.car.systemManage.dao.api;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.magicrule.car.systemManage.model.Permission;

/**
 * 
 * @ClassName:  PermissionDaoApi   
 * @Description:TODO(权限Dao接口,insert/remove/update/get/find)   
 * @author: hebb
 * @date:   2019年2月23日 下午5:28:57   
 *     
 * @Copyright: 2019 www.magicruler.com Inc. All rights reserved. 
 * 注意：本内容仅限于魔尺信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目的
 */
public interface PermissionDaoApi {


	/**
	 * 
	 * @Title: removePermission   
	 * @Description: TODO(删除权限)   
	 * @param: @param permissionList
	 * @param: @return      
	 * @return: int      
	 * @throws
	 */
	int removePermission(@Param("permissionList") List<Permission> permissionList);
	

	/**
	 * 
	 * @Title: updatePermission   
	 * @Description: TODO(修改权限)   
	 * @param: @param permission
	 * @param: @return      
	 * @return: int      
	 * @throws
	 */
	int updatePermission(Permission permission);
	

	/**
	 * 
	 * @Title: insertPermission   
	 * @Description: TODO(插入权限)   
	 * @param: @param permission
	 * @param: @return      
	 * @return: int      
	 * @throws
	 */
	int insertPermission(Permission permission);

	/**
	 * 
	 * @Title: findPermissionList   
	 * @Description: TODO(分页查询权限列表)   
	 * @param: @param permission
	 * @param: @return      
	 * @return: List<Permission>      
	 * @throws
	 */
	List<Permission> findPermissionList(Permission permission);
	
	
	/**
	 * 
	 * @Title: getPermissionById   
	 * @Description: TODO(根据id查询权限)   
	 * @param: @param id
	 * @param: @return      
	 * @return: Permission      
	 * @throws
	 */
	Permission getPermissionById(String id);
	
	
	/**
	 * 
	 * @Title: getPermissionLisByMultiId   
	 * @Description: TODO(根据多个id查询权限)   
	 * @param: @param permissionList
	 * @param: @return      
	 * @return: List<Permission>      
	 * @throws
	 */
	List<Permission> getPermissionLisByMultiId(@Param("permissionList") List<Permission> permissionList);
	
}
