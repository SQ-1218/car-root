package com.magicrule.car.systemManage.service.api;

import java.util.List;

import com.magicrule.car.systemManage.model.Role;
import com.magicrule.car.systemManage.model.RoleResourcePermission;
import com.magicrule.car.systemManage.model.User;

/**
 * 
 * @ClassName:  UserServiceAPi   
 * @Description:TODO(用户Service接口)   
 * @author: hebb
 * @date:   2018年11月28日 下午10:06:25   
 *     
 * @Copyright: 2018 www.magicruler.com Inc. All rights reserved. 
 * 注意：本内容仅限于魔尺信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目的
 */
public interface UserServiceApi {
	
	/**
	 * 
	 * @Title: removeUser   
	 * @Description: TODO(删除用户)   
	 * @param: @param userList
	 * @param: @return      
	 * @return: int      
	 * @throws
	 */
	String removeUser(List<User> userList);
	
	/**
	 * 
	 * @Title: updateUser   
	 * @Description: TODO(修改用户)   
	 * @param: @param user
	 * @param: @return      
	 * @return: int      
	 * @throws
	 */
	int updateUser(User user);
	
	/**
	 * 
	 * @Title: insertUser   
	 * @Description: TODO(新增用户)   
	 * @param: @param user
	 * @param: @return      
	 * @return: int      
	 * @throws
	 */
	int insertUser(User user);

	/**
	 * 
	 * @Title: findUserList   
	 * @Description: TODO(分页查询用户列表)   
	 * @param: @param user
	 * @param: @return      
	 * @return: List<User>      
	 * @throws
	 */
	List<User> findUserList(User user);
	
	
	/**
	 * 
	 * @Title: getByUserName   
	 * @Description: TODO(根据用户名称查找用户)   
	 * @param: @param user
	 * @param: @return      
	 * @return: User      
	 * @throws
	 */
	User getUserByUserName(String userName);
	
	/**
	 * 
	 * @Title: getUserById   
	 * @Description: TODO(根据用户Id查找用户)   
	 * @param: @param userName
	 * @param: @return      
	 * @return: User      
	 * @throws
	 */
	User getUserById(Long id);
	
	/**
	 * 
	 * @Title: getRolesByUserName   
	 * @Description: TODO(根据用户名称查找该用户的角色)   
	 * @param: @param userId
	 * @param: @return      
	 * @return: List<String>      
	 * @throws
	 */
	List<Role> getRoleListByUserId(Long userId);
	
	/**
	 * 
	 * @Title: getPermissionsByRole   
	 * @Description: TODO(根据角色名称查找角色相关的资源)   
	 * @param: @param roleNames
	 * @param: @return      
	 * @return: List<RoleResourcePermission>      
	 * @throws
	 */
	List<RoleResourcePermission> getPermissionsByRoleList(List<Role> roleList);
	
	/**
	 * 
	 * @Title: getUserLisByMultiId   
	 * @Description: TODO(根据多个id查询权限)   
	 * @param: @param userList
	 * @param: @return      
	 * @return: List<User>      
	 * @throws
	 */
	List<User> getUserLisByMultiId(List<User> userList);
	

}
