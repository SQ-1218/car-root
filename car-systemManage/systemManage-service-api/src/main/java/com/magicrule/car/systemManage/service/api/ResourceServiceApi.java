package com.magicrule.car.systemManage.service.api;

import java.util.List;

import com.magicrule.car.systemManage.model.Resource;

/**
 * 
 * @ClassName:  ResourceServiceApi   
 * @Description:TODO(菜单资源Service接口)   
 * @author: hebb
 * @date:   2019年2月22日 下午10:27:16   
 *     
 * @Copyright: 2019 www.magicruler.com Inc. All rights reserved. 
 * 注意：本内容仅限于魔尺信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目的
 */
public interface ResourceServiceApi {

	/**
	 * 
	 * @Title: removeResource   
	 * @Description: TODO(删除菜单资源)   
	 * @param: @param resourceList
	 * @param: @return      
	 * @return: int      
	 * @throws
	 */
	String removeResource(List<Resource> resourceList);
	

	/**
	 * 
	 * @Title: updateResource   
	 * @Description: TODO(修改菜单资源)   
	 * @param: @param resource
	 * @param: @return      
	 * @return: int      
	 * @throws
	 */
	int updateResource(Resource resource);
	

	/**
	 * 
	 * @Title: insertResource   
	 * @Description: TODO(新增菜单资源)   
	 * @param: @param resource
	 * @param: @return      
	 * @return: int      
	 * @throws
	 */
	int insertResource(Resource resource);

	/**
	 * 
	 * @Title: findResourceList   
	 * @Description: TODO(分页查询菜单资源列表)   
	 * @param: @param resource
	 * @param: @return      
	 * @return: List<Resource>      
	 * @throws
	 */
	List<Resource> findResourceList(Resource resource);
	
	
	/**
	 * 
	 * @Title: getResourceById   
	 * @Description: TODO(根据Id查询菜单资源)   
	 * @param: @param id
	 * @param: @return      
	 * @return: Resource      
	 * @throws
	 */
	Resource getResourceById(Long id);
	
	
	/**
	 * 
	 * @Title: getResourceLisByMultiId   
	 * @Description: TODO(根据多个id查询菜单资源)   
	 * @param: @param resourceList
	 * @param: @return      
	 * @return: List<Resource>      
	 * @throws
	 */
	List<Resource> getResourceLisByMultiId(List<Resource> resourceList);
	
	
	/**
	 * 
	 * @Title: getResourceCountByCode   
	 * @Description: TODO(检查菜单编码是否已经被使用)   
	 * @param: @param code
	 * @param: @return      
	 * @return: Integer      
	 * @throws
	 */
	Integer getResourceCountByCode(String code);
	
	
}
