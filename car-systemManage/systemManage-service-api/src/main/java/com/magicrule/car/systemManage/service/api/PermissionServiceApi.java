package com.magicrule.car.systemManage.service.api;

import java.util.List;

import com.magicrule.car.systemManage.model.Permission;

/**
 * 
 * @ClassName:  PermissionServiceApi   
 * @Description:TODO(权限Service接口)   
 * @author: hebb
 * @date:   2019年2月23日 下午5:34:05   
 *     
 * @Copyright: 2019 www.magicruler.com Inc. All rights reserved. 
 * 注意：本内容仅限于魔尺信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目的
 */
public interface PermissionServiceApi {


	/**
	 * 
	 * @Title: removePermission   
	 * @Description: TODO(删除权限)   
	 * @param: @param permissionList
	 * @param: @return      
	 * @return: int      
	 * @throws
	 */
	String removePermission(List<Permission> permissionList);
	

	/**
	 * 
	 * @Title: updatePermission   
	 * @Description: TODO(修改权限)   
	 * @param: @param permission
	 * @param: @return      
	 * @return: int      
	 * @throws
	 */
	int updatePermission(Permission permission);
	

	/**
	 * 
	 * @Title: insertPermission   
	 * @Description: TODO(插入权限)   
	 * @param: @param permission
	 * @param: @return      
	 * @return: int      
	 * @throws
	 */
	int insertPermission(Permission permission);

	/**
	 * 
	 * @Title: findPermissionList   
	 * @Description: TODO(分页查询权限列表)   
	 * @param: @param permission
	 * @param: @return      
	 * @return: List<Permission>      
	 * @throws
	 */
	List<Permission> findPermissionList(Permission permission);
	
	
	/**
	 * 
	 * @Title: getPermissionById   
	 * @Description: TODO(根据Id查询权限)   
	 * @param: @param id
	 * @param: @return      
	 * @return: Permission      
	 * @throws
	 */
	Permission getPermissionById(String id);
	
	
	/**
	 * 
	 * @Title: getPermissionLisByMultiId   
	 * @Description: TODO(根据多个id查询权限)   
	 * @param: @param permissionList
	 * @param: @return      
	 * @return: List<Permission>      
	 * @throws
	 */
	List<Permission> getPermissionLisByMultiId(List<Permission> permissionList);
	
}
