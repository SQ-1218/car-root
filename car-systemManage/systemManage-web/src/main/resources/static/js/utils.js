jQuery.fn.load = function( url, params, callback ) {
	if ( typeof url !== "string" && _load ) {
		return _load.apply( this, arguments );
	}

	// Don't do a request if no elements are being requested
	if ( !this.length ) {
		return this;
	}

	var selector, type, response,
		self = this,
		off = url.indexOf(" ");

	if ( off >= 0 ) {
		selector = url.slice( off, url.length );
		url = url.slice( 0, off );
	}

	// If it's a function
	if ( jQuery.isFunction( params ) ) {

		// We assume that it's the callback
		callback = params;
		params = undefined;

	// Otherwise, build a param string
	} else if ( params && typeof params === "object" ) {
		type = "POST";
	}

	// Request the remote document
	jQuery.ajax({
		url: url,

		// if "type" variable is undefined, then "GET" method will be used
		type: type,
		dataType: "html",
		data: params,
		complete: function( jqXHR, status ) {
			var responseText=jqXHR.responseText;
			if ( callback ) {
				self.each( callback, response || [ jqXHR.responseText, status, jqXHR ] );
			}
			
			// Save response for use in complete callback
			response = arguments;

			// See if a selector was specified
			self.html( selector ?

				// Create a dummy div to hold the results
				jQuery("<div>")

					// inject the contents of the document in, removing the scripts
					// to avoid any 'Permission Denied' errors in IE
					.append( responseText.replace( rscript, "" ) )

					// Locate the specified elements
					.find( selector ) :

				// If not, just inject the full result
				responseText );
		}
	});

	return this;
};

$.ajaxSetup({cache:false,timeout:1800000,beforeSend:function(jqXHR, settings){
	if(settings.url.indexOf("?")>0){
		settings.url+="&boolAjax=true";
	}else{
		settings.url+="?boolAjax=true";
	}
}
});

$(document).ajaxSuccess(function(evt,request,setting){
	var responseText=request.responseText;
	if(responseText=='timeoutError'){
		top.openLogin(null);
		request.responseText="登录超时,请重新登录。";
		request.abort();
	}
});

//限制访问路径
/*
var topurl=window.top.location.href;
if(!/.+[3cs\/index.do]$/.test(topurl))
	window.top.location.href="/3cs/index.do";
*/

$.isEmptyStr=function(str){
	if(str==undefined)
		return true;
	else if(str==null)
		return true;
	else if(jQuery.trim(str)=='')
		return true;
	else 
		return false;
};

$.formatNumber = function(num, scale, def){
	if (typeof num != 'number') {
		num = parseFloat(num);
		if (isNaN(num)) {
			num = def;
		}
	}
	return num.toFixed(scale);
};

//日期参数
var date_parameter={
		numberOfMonths: 2,
		showButtonPanel: true,
		dateFormat:"yy-mm-dd",
		yearRange:"c-5:c+3",
		changeYear:true,
		changeMonth:true
	};

function jsonArrayToStr(adata){
	if(adata.length>0){
		var len=1;
		var result="";
		$.each(adata,function(a,ad){
			if(!$.isEmptyObject(ad)){
			if(len<adata.length){
			result+=$.param(ad)+"|";
			}else{
			result+=$.param(ad);
			}
			}
		});
		return result;
	}
	
}
function setSeletedValue(showV,hideV,codeArray,nameArray){
	var prv=$("#"+hideV).val();
	var result=codeArray.toString();
	var names=nameArray.toString();
	for(var m=0;m<codeArray.length;m++){
	     if(codeArray[m]=="all"){
	         result='';
	         break;
	      }
	}
	$("#"+showV).val(names);
	$("#"+hideV).val(result);
	if(prv!=result){
		$("#"+hideV).trigger("change");
		$("#"+showV).trigger("change");
	}
}

function getSeletedValue(objId){
	return $("#"+objId).val();
	
}


Array.prototype.removeByValue=function(val){
	for(var i=0;i<this.length;i++){
		if(this[i]==val){
			this.splice(i,1);
			break;
		}
	}
};

Array.prototype.removeByIndex=function(index){
	this.splice(index,1);
};


function uniqueArray(data){
	data=data||[];
	var a={};
	len=this.length;
	for(var i=0;i<data.length;i++){
		var v=data[i];
		if(typeof(a[v]=='undefined')){
			a[v]=1;
		}
	};
	data.length=0;
	for(var i in a){
		data[data.length]=i;
	}
	return data;
};

function setNumValue(obj,event,qname) {
	event= event || window.event;
	 if ((event.keyCode >= 48 && event.keyCode <= 57) ||(event.keyCode >= 96 && event.keyCode <= 105) || event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 35 || event.keyCode == 36 ) {
	  
	    } else if(event.keyCode == 13){
	    	if($.isEmptyStr(obj.value)||obj.value=='0')
	    		obj.value=1;
	    	goPage(obj,'topage',obj.value,qname);
	    }else {
	if(document.all) {
	        return false;//IE
	}else{
		event.preventDefault();//FF
	}
	    }
	}

//移除frame的等待提示
function removeFrameWaiting(fid){
	$("#"+fid).parent().removeWait();
}

/////////////////////
//初始化排序,参数nn表示qPrams中的第几个
function InitSort(nn,qname){
	if(qname==undefined || qname=='')
	qname='qPrams';
	var gridDiv=$("#"+nn);
	gridDiv.find("thead td").each(function(i){
		var sort=$(this).attr("sort");
		var tds=$(this);
		if(sort!="" && sort!=undefined){
			$(this).bind("click",function(){queryWithSort(tds,sort,nn,qname);});
			$(this).attr("style","cursor:pointer");
		}
	});
	setSortClass(nn,qname);
}

//设置sort样式
function setSortClass(nn,qname){
	var gridDiv=$("#"+nn);
	if(eval(qname)[nn].sortparams.sort_order!=undefined && eval(qname)[nn].sortparams.sort_order!=''){
		gridDiv.find("thead td").each(function(i){
			var sort=$(this).attr("sort");
			var spans=$(this).find("span").length;
			if(sort==eval(qname)[nn].sortparams.sort_order){
				if(spans>0)
					$(this).find("span").attr("class","datagrid-sort-"+eval(qname)[nn].sortparams.sort_Type);
				else
					$(this).append("<span class='datagrid-sort-"+eval(qname)[nn].sortparams.sort_Type+"'></span>");
			}else{
				if(spans>0)
					$(this).find("span").hide();
			}
		});
	}
		
}
//排序查询
function queryWithSort(obj,sv,nn,qname){
	var gridDiv=$("#"+nn);
	var spansort=obj.find("span");
	if(spansort.length>0){
	if(spansort.attr("class")=='datagrid-sort-desc'){
		eval(qname)[nn].sortparams.sort_Type="asc";
		spansort.attr("class","datagrid-sort-asc");
		}else{
		eval(qname)[nn].sortparams.sort_Type="desc";
		spansort.attr("class","datagrid-sort-desc");
		}
	}else{
		eval(qname)[nn].sortparams.sort_Type="asc";
		spansort.attr("class","datagrid-sort-asc");
	}
	eval(qname)[nn].sortparams.sort_order=sv;

	eval(qname)[nn].pageNum=1;
	var pageParam={"ajaxPage":true,"intCurrentPage":eval(qname)[nn].pageNum,"intPageSize":eval(qname)[nn].pageSize};
	eval(qname)[nn].pageparams=pageParam;
	queryWithPage(gridDiv,eval(qname)[nn].url,eval(qname)[nn].queryparams,eval(qname)[nn].pageparams,eval(qname)[nn].sortparams);
	//gridDiv.find("#refreshPage").click();
}

//分页查询，参数nn表示qPrams中的第几个
function queryWithPageFast(nn,qname){
	if(qname==undefined || qname=='')
	qname='qPrams';
	
	var gridDiv=$("#"+nn);
	//设置pageparams，起始页，一页个数
	eval(qname)[nn].pageNum=1;
	var pageParam={"ajaxPage":true,"intCurrentPage":eval(qname)[nn].pageNum,"intPageSize":eval(qname)[nn].pageSize};
	eval(qname)[nn].pageparams=pageParam;
	queryWithPage(gridDiv,eval(qname)[nn].url,eval(qname)[nn].queryparams,eval(qname)[nn].pageparams,eval(qname)[nn].sortparams);
}

function theQueryParam(nn,qname){
	var qp;
	if(jQuery.isPlainObject(eval(qname)[nn].sortparams))
		qp=$.extend({},eval(qname)[nn].queryparams,eval(qname)[nn].pageparams,eval(qname)[nn].sortparams);
	else
		qp=$.extend({},eval(qname)[nn].queryparams,eval(qname)[nn].pageparams);	

	return qp;
}

//得到分页查询的所有参数{....}
function queryWithPageParams(nn,qname){
	if(qname==undefined || qname=='')
		qname='qPrams';
		
		//设置pageparams，起始页，一页个数
		eval(qname)[nn].pageNum=1;
		var pageParam={"ajaxPage":true,"intCurrentPage":eval(qname)[nn].pageNum,"intPageSize":eval(qname)[nn].pageSize};
		eval(qname)[nn].pageparams=pageParam;
		var queryparams=eval(qname)[nn].queryparams;
		var pageparams=eval(qname)[nn].pageparams;
		var sortparams=eval(qname)[nn].sortparams;
		var qp;
		if(jQuery.isPlainObject(sortparams))
			qp=$.extend({},queryparams,pageparams,sortparams);
		else
			qp=$.extend({},queryparams,pageparams);	
		return qp;
}

function queryWithPage(gridDiv,url,queryparams,pageparams,sortparams){
	var qp;
	if(jQuery.isPlainObject(sortparams))
		qp=$.extend({},queryparams,pageparams,sortparams);
	else
		qp=$.extend({},queryparams,pageparams);	

	gridDiv.showWait();
	gridDiv.load(url,qp);
}


//跳转到页面函数
function goPage(obj,type,countP,qname){
	if(qname==undefined || qname=='')
	qname='qPrams';
	var griddiv=$(obj).parents(".right_grid_block");
	if($(obj).hasClass("l-btn-disabled"))
		return;
	griddiv.find(".pagination-loading").css("display","block");
	var pageParam={};
	var nn=griddiv.attr("id");

	if(type=="pageSize"){
		eval(qname)[nn].pageSize=obj.value;
	}else if(type=="first"){
		eval(qname)[nn].pageNum=1;
	}else if(type=="prev"){
		eval(qname)[nn].pageNum=parseInt(eval(qname)[nn].pageNum)-1;
	}else if(type=="next"){
		eval(qname)[nn].pageNum=parseInt(eval(qname)[nn].pageNum)+1;
	}else if(type=="last"){
		eval(qname)[nn].pageNum=countP;
	}else if(type=="topage"){
		var pgv=obj.value;
		if(pgv>countP)
			eval(qname)[nn].pageNum=countP;
		else	
			eval(qname)[nn].pageNum=pgv;
		
	}
	pageParam={"ajaxPage":true,"intCurrentPage":eval(qname)[nn].pageNum,"intPageSize":eval(qname)[nn].pageSize};
	eval(qname)[nn].pageparams=pageParam;
	
	setTimeout(function() {
		queryWithPage(griddiv,eval(qname)[nn].url,eval(qname)[nn].queryparams,eval(qname)[nn].pageparams,eval(qname)[nn].sortparams);
	}, 150);
}


function tojson(str){
	return eval('(' + str + ')');
}

//移除遮罩层
function removeWaitById(wid){
	$("#"+wid).removeWait();
}

//搜索
function searchkey(textid,pd){
	var key=$.trim($('#'+textid).val());
	if(key!=''){
		//清除原来搜索内容
		$("#"+pd).find('.gridview').find('.bodycell').each(function(i){
			$(this).css("background-color","");
		});
		//重新搜索内容
		$("#"+pd).find('.gridview').find('.bodycell').each(function(i){
			var tt=$(this).text();
			var tt1=tt.toLowerCase();
			if(tt1.indexOf(key.toLowerCase())>-1){
				$(this).attr('style','background-color:#BFE3F1;');
				return true;
				}
			});
		
		$("#"+pd).find('.gridview').find('.sonspan').each(function(i){
			var tt=$(this).text();
			var tt1=tt.toLowerCase();
			if(tt1.indexOf(key.toLowerCase())>-1){
				$(this).attr('style','background-color:#BFE3F1;');
				return true;
				}
			});
	}else{
		$("#"+pd).find('.gridview').find('.bodycell').each(function(i){
			$(this).css("background-color","");
		});
		
		$("#"+pd).find('.gridview').find('.sonspan').each(function(i){
			$(this).css("background-color","");
			});
	}
	}

//关闭窗口
function close_win(wid){
	$("#"+wid).window("close");
	$("#"+wid).html("<div><img src='${ctx }/css/login/images/loading.gif' class='loadimage'/>正在加载,请稍候...</div>");
}

function allCheck_grid(obj,gridId){
	 if(obj.checked){
	$("#"+gridId).find("tbody").find(":checkbox").attr("checked",true);
		}else{
	$("#"+gridId).find("tbody").find(":checkbox").attr("checked",false);
		}
}

function onlyCheck_grid(cobj,gridId){
	var cname=cobj.name;
$("#"+gridId+" :checkbox").each(function(ci){
	if(this.name==cname){
		if(this.checked){
		this.checked=true;
		}
	}else{
		this.checked=false;
	}
	});
}

//导出excel
function exportToExcel(gridoo,urlexp){
	var cc=parseInt(gridoo.find(".allcount").text());
	if(cc>100000){
		$('#dialog-confirm').dialog({ 
		      title:"导出数据", 
		      width:200,   
		      height:150,
		      content:"要导出的数据已超过100000条,是否继续导出?",   
		      shadow:false,
		      minimizable:false,
		      modal:true,
		      buttons:[{
					text:'确定',
					iconCls:'icon-ok',
					handler:function(){
						$("#exportXls", window.top.document).attr("src", urlexp);
						$('#dialog-confirm').dialog('close');
					}
				},{
					text:'取消',
					iconCls:'icon-undo',
					handler:function(){
						$('#dialog-confirm').dialog('close');
					}
				}]
		  });
	}else{
		$("#exportXls", window.top.document).attr("src", urlexp);
	}
	}


(function($) {
	
	jQuery.fn.enterKeyChange=function(){
		this.bind("keypress",function(event){
			var keyCode=event.keyCode?event.keyCode:event.which?event.which:event.charCode;
			//回车事件触发change
			if(keyCode==13)
				$(this).trigger("change");
		});
	};
	
	//cols表示第几列，从0开始,darray表示列对应的值,carray表示那几列是钱,cnum表示有表格共多少列
	jQuery.fn.tableSumData=function(cols,darray,carray,cnum){
		if(cols.length<0)
			return false;
		var lentd=0;
		if($.isEmptyStr(cnum))
		lentd=this.find(".headtr td").length-1;
		else
		lentd=cnum;	
		var tby=this.find(".gridview tbody");
		var gtr=$("<tr><td class='bodycell'>合计:</td></tr>");
		if(this.find(".no_datatd").length>0){
			return false;
		}
		if(this.find(".no_data").length>0){
			return false;
		}
		
		for(var v=0;v<lentd;v++){
			var gtd=$("<td class='bodycell'></td>");
			gtr.append(gtd);
		}

		for(var i=0;i<cols.length;i++){
			var n=cols[i];
			var bool=false;
			if(carray!=undefined){
			for(var k=0;k<carray.length;k++){
				if(carray[k]==n){
					bool=true;
					break;
				}
			}
			}
			var tdobj=gtr.find("td").eq(n);
			if(bool){
				tdobj.addClass("rightCurrency");
				var currency=formatCurrency(darray[i]);
				tdobj.html(currency);
			}else{
				tdobj.addClass("right");
				tdobj.text(parseFloat(darray[i]).toFixed(2));
			}
			
		}
		tby.append(gtr);
	};
	
	//cols表示第几列，从0开始
	jQuery.fn.tableSum=function(cols,rspan){
		if(cols.length<0)
			return false;
		var varray=new Array();
		for(var i=0;i<cols.length;i++){
			varray[i]=0.00;
		}
		var lentd=this.find(".headtr td").length-1;
		var tby=this.find(".gridview tbody");
		var gtr=$("<tr><td class='bodycell'>合计:</td></tr>");
		if(this.find(".no_datatd").length>0){
			return false;
		}
		if(this.find(".no_data").length>0){
			return false;
		}

		tby.find("tr").each(function(t){
			for(var i=0;i<cols.length;i++){
				var m=cols[i];
				var vv='';
				if(!$.isEmptyStr(rspan)){
					var len=$(this).find("td.rowspan").length;
					m = m-(rspan-len);
				}else{
					vv=$(this).find("td").eq(m).text();
				}
				if(vv==''||vv==undefined){
					vv=0.00;
				}
				varray[i]+=parseFloat(vv);
			}
		});
		
		for(var v=0;v<lentd;v++){
			var gtd=$("<td class='bodycell'></td>");
			gtr.append(gtd);
		}
		for(var i=0;i<cols.length;i++){
			var n=cols[i];
			gtr.find("td").eq(n).text(varray[i].toFixed(2));
		}
		tby.append(gtr);
	};
	
	//普通查询
	jQuery.fn.getDialogQuery=function(){
		var nq={};
		this.find(".toolbar_table td.td_left").each(function(df){
			var len=$(this).find("select").length;
			if(len>0){
				var k=$(this).find("select").eq(0).val();
				var v=$(this).next().find(".textinput").eq(0).val();
				if(!$.isEmptyStr(v) && !$.isEmptyStr(k))
				nq[k]=v;
			}else{
				$(this).next().find(".textinput").each(function(){
				var k=this.name;
				var v=this.value;
				if(k=='keyword'){
					var t=$(this).attr("title");
					if(v==t)
						v="";
				}
				if(!$.isEmptyStr(v) && !$.isEmptyStr(k))
				nq[k]=v;
				});
			}	
		});
		
		return nq;
	};
	
	function handleXChar(vv){
		vv=vv.replace(/\[/g,"[[]");
		vv=vv.replace(/%/g,"[%]");
		vv=vv.replace(/_/g,"[_]");
		vv=vv.replace(/\^/g,"[^]");
		vv=vv.replace(/'/g,"''");
		return vv;
	}
	
	//普通查询
	jQuery.fn.getNormalQuery=function(){
		var nq={};
		this.find(".toolbar_table td").each(function(df){
			var len=$(this).find(".qspan").length;
			if(len>0){
				var k=$(this).find(".qspan").find("select").eq(0).val();
				var v=$(this).find(".textinput").eq(0).val();
				var tt=$(this).find(".textinput").eq(0).has("notransfer");
				if(!$.isEmptyStr(v) && !$.isEmptyStr(k)){
					if(!tt)
						nq[k]=v;
					else
						nq[k]=handleXChar(v);
				}
				
			}else{
				$(this).find(".textinput").each(function(){
				var k=this.name;
				var v=this.value;
				if(k=='keyword'){
					var t=$(this).attr("title");
					if(v==t)
						v="";
				}
				if($(this).attr("class").indexOf("datebox")>-1)
					v=$(this).datebox("getValue");
				if(!$.isEmptyStr(v) && !$.isEmptyStr(k)){
					if($(this).hasClass("notransfer"))
						nq[k]=v;
					else
						nq[k]=handleXChar(v);
				}
				});
			}	
		});
		
		return nq;
	};

	//高级查询
	jQuery.fn.getSeniorQuery=function(){
		var nq={};
		this.find("tr").each(function(df){
			var len=$(this).find(".td_left").find(".qspan").length;
			if(len>0){
				var k=$(this).find(".td_left").find("select").eq(0).val();
				var v=$(this).find(".td_right").find(".seniorinput").eq(0).val();
				var tt=$(this).find(".textinput").eq(0).has("notransfer");
				if(!$.isEmptyStr(v) && !$.isEmptyStr(k)){
					if(!tt)
						nq[k]=v;
					else
						nq[k]=handleXChar(v);
				}
			}else{
				$(this).find(".td_right .seniorinput").each(function(){
				var k=this.name;
				var v=this.value;
				if(k=='keyword'){
					var t=$(this).attr("title");
					if(v==t)
						v="";
				}
				if(this.type=='checkbox'||this.type=='radio'){
					if(!this.checked)
						v="";
				}
				if($(this).attr("class").indexOf("datebox")>-1)
					v=$(this).datebox("getValue");
				if(!$.isEmptyStr(v) && !$.isEmptyStr(k)){
					if($(this).hasClass("notransfer"))
						nq[k]=v;
					else
						nq[k]=handleXChar(v);
				}
				});
			}	
		});
		return nq;
	};
	
	//初始化 网格
	jQuery.fn.initGridView=function(){
		this.find("tbody tr").unbind();
		this.find("tbody tr").bind("mouseover",function(){
			$(this).addClass("hoverrow");
		});
		this.find("tbody tr").bind("mouseout",function(){
			$(this).removeClass("hoverrow");
		});
		this.find("tbody tr :checkbox").bind("click",function(event){
			event.stopPropagation();
		});
		this.find("tbody tr").bind("click",function(){
			if($(this).parent().find("tr.selectrow").length>0)
				$(this).parent().find("tr.selectrow").removeClass("selectrow");
				$(this).addClass("selectrow");
				
			var checkobj=$(this).find(":checkbox:first");
			if(checkobj.length>0){
				checkobj[0].click();
				/*
				if(checkobj.attr("checked")=='checked'){
					checkobj.removeAttr("checked");
				}else{
					checkobj.attr("checked","checked");
				}*/
			}
		});
	};
	
	jQuery.fn.initRoleGrid=function(){
		this.find("tbody tr").unbind();
		this.find("tbody tr").bind("mouseover",function(){
			$(this).addClass("hoverrow");
		});
		this.find("tbody tr").bind("mouseout",function(){
			$(this).removeClass("hoverrow");
		});
		this.find("tbody tr :checkbox").bind("click",function(event){
			event.stopPropagation();
		});
		this.find("tbody tr").bind("click",function(){
			if($(this).parent().find("tr.selectrow").length>0)
				$(this).parent().find("tr.selectrow").removeClass("selectrow");
				$(this).addClass("selectrow");
		});
	};
	
	
	//判断是否重复行
	jQuery.fn.isRepeatTr=function(trobj){
		var booladd=flase;
		this.find("input:hidden").each(function(ii){
			if(this.value==trv){
				booladd=true;
				return false;
			}
			});
		return booladd;
	};
	
	jQuery.fn.addSelectedRow=function(trobj,boolrow){
		var trv=$(trobj).find("input:hidden").val();
		var tid=this.attr("id");
		var gridBody=this.find("tbody");
		var booladd=true;
		if(boolrow){
		this.find("input:hidden").each(function(ii){
			if(this.value==trv){
				booladd=false;
				return false;
			}
			});
		}
		
		if(booladd){
			if(this.parent().find(".no_data").length>0)
				this.parent().find(".no_data").remove();
		var selectedtr=$("<tr ondblClick=$('#"+tid+"').deleteSelectedRow(this);></tr>");
		selectedtr.html($(trobj).html());
		gridBody.append(selectedtr);
		
		gridBody.find("tr").each(function(r){
			$(this).find(".spantext").text(r+1);
			});
		
			return true;
	 	}else{
	 		return false;
	 	}
	};
	
	jQuery.fn.deleteSelectedRow=function(dtrobj){
		$(dtrobj).remove();
		var gridBody=this.find("tbody");
		if(gridBody.find("tr").length>0){
		gridBody.find("tr").each(function(r){
			$(this).find(".spantext").text(r+1);
			});
		}else{
			this.after("<div class='no_data'>对不起！目前没有数据，请选择数据。</div>");
		}
	};

	//获取所选行数据，逗号分隔
	jQuery.fn.getSelectedRow=function(){
		var len=this.find("input:hidden").length-1;
		var restr="";
		this.find("input:hidden").each(function(ii){
			if(ii<len)
			restr+=this.value+",";
			else
			restr+=this.value;	
		});
		return restr;
	};
	
	//填充操作记录信息
	jQuery.fn.fillOperation=function(uname,formcode,optTime,optName){
		this.find(".tdbj_right").eq(0).text(uname);
		this.find(".tdbj_right").eq(1).text(formcode);
		this.find(".tdbj_right").eq(2).text(optTime);
		this.find(".tdbj_right").eq(3).text(optName);
	};
	
	/*关键词提示*/
	jQuery.fn.inputTip=function(){
		var keyt=this.attr("title");
		if(keyt.length>0){
			this.css({"color":"#DDDDDD"});
			this.val(keyt);
		}
		this.bind("click",function(){
			if($(this).val()==$(this).attr("title")){
			$(this).css({"color":"#000000"});
			$(this).val("");
			}
		});
		this.bind("blur",function(){
			if($.trim($(this).val())==''){
			$(this).css({"color":"#DDDDDD"});
			$(this).val($(this).attr("title"));
			}
		});
	};
	
	//所有元素只读
	jQuery.fn.setReadonly=function(rn){
		this.find(":input:not(:disabled)").each(function(nd){
			if(rn)
			$(this).attr("readonly","readonly");
			else
			$(this).removeAttr("readonly");
		});
	};
	
	//显示按钮
	jQuery.fn.showElem=function(){
		if(this.length>0){
		var tid=this.attr("id");
		this.show();
		if($("#"+tid+"_td").length>0)
		$("#"+tid+"_td").show();
		}
	};
	
	//隐藏按钮
	jQuery.fn.hideElem=function(){
		if(this.length>0){
		var tid=this.attr("id");
		this.hide();
		if($("#"+tid+"_td").length>0)
		$("#"+tid+"_td").hide();
		}
	};
	
	//启用按钮
	jQuery.fn.enableElem=function(){
		if(this.length>0){
		var tname=this[0].nodeName;
		var tda;
		if("A"!=tname)
			tda=this.find("a");
		else
			tda=this;
		if($.isEmptyStr(tda.attr("onclick"))){
		tda.removeAttr("style");
		tda.find("span:first").removeClass("icons16-disable");
		tda.find("span:first").addClass("icons16");
		var cfun=tda.attr("onclickStr");
		tda.attr("onclick",cfun);
		}
		}
	};

	//禁用按钮
	jQuery.fn.disableElem=function(){
		if(this.length>0){
		var tname=this.nodeName;
		var tda;
		if("A"!=tname)
			tda=this.find("a");
		else
			tda=this;
		if(!$.isEmptyStr(tda.attr("onclick"))){
		tda.attr("style","color:#bbbbbb;cursor:not-allowed;");
		tda.find("span:first").removeClass("icons16");
		tda.find("span:first").addClass("icons16-disable");
		tda.removeAttr("onclick");
		}
		}
	};
	
	jQuery.fn.oneKeyDate=function(selectFun){
		var dateobj=this;
		var dateN=this.attr("name");
		var did=dateobj.attr("id");
		this.datebox({onChange:function(nv,ov){
			dateobj.attr("value",nv);
			$("#"+did+"Tip").tipCorrect();
			//dateobj.trigger("blur");
		},
		onSelect:function(date){
			if($.isFunction(selectFun)){
				selectFun.call(date);
			}
		},
		formatter:function(date){
			var y=date.getFullYear();
			var m=date.getMonth()+1;
			var d=date.getDate();
			if(m<10){
				m='0'+m;
			}
			if(d<10){
				d='0'+d;
			}
			return y+'-'+m+'-'+d;
		}});
		
		this.each(function(){
			$(this).combo("textbox").date("y-M-d");
			if($(this).attr("class").indexOf('textinput')>-1 || $(this).attr("class").indexOf('seniorinput ')>-1){
				dateobj.attr("name",dateN);
				}
		});
		
	};
	
	jQuery.fn.oneKeyDateTime=function(selectFun){
		var dateobj=this;
		var dateN=this.attr("name");
		var did=dateobj.attr("id");
		this.datetimebox({onChange:function(nv,ov){
			dateobj.attr("value",nv);
			$("#"+did+"Tip").tipCorrect();
			//dateobj.trigger("blur");
			},
			onSelect:function(date){
				if($.isFunction(selectFun)){
					selectFun.call(date);
				}
			},
			formatter:function(date){
				var y=date.getFullYear();
				var mon=date.getMonth()+1;
				var d=date.getDate();
				var h=date.getHours();
				var m=date.getMinutes();
				var s=date.getSeconds();
				if(mon<10){
					mon='0'+mon;
				}
				if(d<10){
					d='0'+d;
				}
				if(h<10){
					h='0'+h;
				}
				if(m<10){
					m='0'+m;
				}
				if(s<10){
					s='0'+s;
				}
				return y+'-'+mon+'-'+d+' '+h+':'+m+':'+s;
			}});
		
		this.each(function(){
		$(this).combo("textbox").date("y-M-d H:m:s");
		if($(this).attr("class").indexOf('textinput')>-1 || $(this).attr("class").indexOf('seniorinput ')>-1){
			dateobj.attr("name",dateN);
		}
		});
	};
	
	jQuery.fn.tipInfo=function(info){
		this.attr("style","margin: 0px; padding: 0px; background: none repeat scroll 0% 0% transparent;");
		this.attr("class","onError");
		this.html("<p class='noticeWrap'><b class='ico-warning'></b><span class='txt-err'>"+info+"</span></p>");
	};
	jQuery.fn.tipCorrect=function(){
		this.attr("style","margin: 0px; padding: 0px; background: none repeat scroll 0% 0% transparent;");
		this.attr("class","onCorrect");
		this.html("<p class='noticeWrap'><b class='ico-succ'></b><span class='txt-succ'>输入正确</span></p>");
	};
	
	//刷新网格
	jQuery.fn.refreshGrid=function(){
	  this.find("#refreshPage").click();
	};
	
	//表单数据对象化,对于多选checkbox，将用逗号分隔值
	jQuery.fn.objParam=function(){
		this.find(":focus").blur();
		var data={};
		if(this.length>0){
		var fields =this.serializeArray();
		jQuery.each( fields, function(i, field){
			var key=field.name;
			var keyvalue=$.trim(field.value);
			if(!$.isEmptyStr(keyvalue) && !$.isEmptyStr(key)){
				if(data[key]==undefined)
					data[key]=keyvalue;
				else
					data[key]=data[key]+","+keyvalue;	
			}
		});
		}
		return data;
	};
	
	/*显示等待提示*/
	jQuery.fn.showWait=function(){
		var toleft=(this.width()-150)/2;
	if(this.find("div.loading-mask").length==0){
	$("<div class='loading-mask'></div>").css({display:"block"}).appendTo(this);
	$("<div class='loading-msg'></div>").html("正在处理，请稍候...").appendTo(this).css({display:"block",left:toleft});
	
	}
	};
	
	/*显示等待提示,带遮罩层偏移*/	
	jQuery.fn.showWaitPy2=function(){
		var toleft=(this.width()-150)/2;
	if(this.find("div.loading-mask").length==0){
	$("<div class='loading-mask'></div>").css({display:"block"}).appendTo(this).css({top:27,left:0});
	$("<div class='loading-msg'></div>").html("正在处理，请稍候...").appendTo(this).css({display:"block",left:toleft});
	
	}
	};
	
	jQuery.fn.showWaitFixed=function(){
		var offset = this.offset();
		var toleft=offset.left+(this.width()-150)/2;
		var totop=offset.top+(this.height()-16)/2;
		var _w=this.width();
		var _h=this.height();
	if(this.find("div.loading-mask3").length==0){
	$("<div class='loading-mask3'></div>").css({display:"block",left:offset.left,top:offset.top,width:_w,height:_h}).appendTo(this);
	$("<div class='loading-msg3'></div>").html("正在加载，请稍候...").appendTo(this).css({display:"block",left:toleft,top:totop});
	
	}
	};
	
	/*iframe显示等待提示*/
	jQuery.fn.showWaitFrame=function(){
		var toleft=(this.width()-150)/2;
	if(this.find("div.loading-mask2").length==0){
	$("<div class='loading-mask2'></div>").css({display:"block"}).appendTo(this);
	$("<div class='loading-msg'></div>").html("正在加载，请稍候...").appendTo(this).css({display:"block",left:toleft});
	
	}
	};
	
	/*移除等待提示*/
	jQuery.fn.removeWait=function(){
		if(this.find("div.loading-mask").length>0){
		this.find("div.loading-mask").remove();
		}
		if(this.find("div.loading-mask2").length>0){
		this.find("div.loading-mask2").remove();
		}
		if(this.find("div.loading-mask3").length>0){
		this.find("div.loading-mask3").remove();
		}
		if(this.find("div.loading-msg3").length>0){
		this.find("div.loading-msg3").remove();
		}
		if(this.find("div.loading-msg").length>0){
		this.find("div.loading-msg").remove();
		}
		};

	
})(jQuery);

/******************************/


$.invalid = function(d){
	return typeof d == 'undefined' || d == null;
};

$.arrParam = function(arr, ic, f){
	var d, m, k = {}, a = [], sort = 1;
	if ($.isFunction(ic)) {
		f = ic;
		ic = undefined;
	}
	for ( var i in arr) {
		d = arr[i];
		if ($.isFunction(f) && f(d) == false)
			continue;
		if ($.invalid(ic)) {
			k = {};
			k.sort = sort++;
			for ( var j in d) {
				k[j] = encodeURIComponent(d[j]);
			}
		} else {
			k = {};
			// add sort
			k.sort = sort++;
			for ( var j in ic) {
				m = ic[j];
				var dm = d[m];
				if (typeof dm == 'undefined' || dm == null)
					k[m] = '';
				else
					k[m] = encodeURIComponent(dm);
			}
		}
		a.push($.param(k, true));
	}
	return a;
};

//合并单元格，调用：
	//$('#process').mergeCell({ 
	// 目前只有cols这么一个配置项, 用数组表示列的索引,从0开始 
	// 然后根据指定列来处理(合并)相同内容单元格 
	//	cols: [0, 3] 
	//	});
;(function($) { 
	// 看过jquery源码就可以发现$.fn就是$.prototype, 只是为了兼容早期版本的插件 
	// 才保留了jQuery.prototype这个形式 
	$.fn.mergeCell = function(options) { 
	return this.each(function() { 
	var cols = options.cols; 
	for ( var i = cols.length - 1; cols[i] != undefined; i--) { 
	// fixbug console调试 
	// console.debug(cols[i]); 
	mergeCell($(this), cols[i]); 
	} 
	dispose($(this)); 
	}); 
	}; 
	// 如果对javascript的closure和scope概念比较清楚, 这是个插件内部使用的private方法 
	// 具体可以参考本人前一篇随笔里介绍的那本书 
	function mergeCell($table, colIndex) { 
	$table.data('col-content', ''); // 存放单元格内容 
	$table.data('col-rowspan', 1); // 存放计算的rowspan值 默认为1 
	$table.data('col-td', $()); // 存放发现的第一个与前一行比较结果不同td(jQuery封装过的), 默认一个"空"的jquery对象 
	$table.data('trNum', $('tbody tr', $table).length); // 要处理表格的总行数, 用于最后一行做特殊处理时进行判断之用 
	// 我们对每一行数据进行"扫面"处理 关键是定位col-td, 和其对应的rowspan 
	$('tbody tr', $table).each(function(index) { 
	// td:eq中的colIndex即列索引 
	var $td = $('td:eq(' + colIndex + ')', this); 
	// 取出单元格的当前内容 
	var currentContent = $td.html(); 
	// 第一次时走此分支 
	if ($table.data('col-content') == '') { 
	$table.data('col-content', currentContent); 
	$table.data('col-td', $td); 
	} else { 
	// 上一行与当前行内容相同 
	if ($table.data('col-content') == currentContent) { 
	// 上一行与当前行内容相同则col-rowspan累加, 保存新值 
	var rowspan = $table.data('col-rowspan') + 1; 
	$table.data('col-rowspan', rowspan); 
	// 值得注意的是 如果用了$td.remove()就会对其他列的处理造成影响 
	$td.hide(); 
	// 最后一行的情况比较特殊一点 
	// 比如最后2行 td中的内容是一样的, 那么到最后一行就应该把此时的col-td里保存的td设置rowspan 
	if (++index == $table.data('trNum')) 
	$table.data('col-td').attr('rowspan', $table.data('col-rowspan')); 
	} else { // 上一行与当前行内容不同 
	// col-rowspan默认为1, 如果统计出的col-rowspan没有变化, 不处理 
	if ($table.data('col-rowspan') != 1) { 
	$table.data('col-td').attr('rowspan', $table.data('col-rowspan')); 
	} 
	// 保存第一次出现不同内容的td, 和其内容, 重置col-rowspan 
	$table.data('col-td', $td); 
	$table.data('col-content', $td.html()); 
	$table.data('col-rowspan', 1); 
	} 
	} 
	}); 
	} 
	// 同样是个private函数 清理内存之用 
	function dispose($table) { 
	$table.removeData(); 
	} 
	})(jQuery); 


function formatCurrency(num) {
      num = num.toString().replace(/\$|\,/g,'');
      if(isNaN(num))
      num = "0";
      sign = (num == (num = Math.abs(num)));
      num = Math.floor(num*100+0.50000000001);
      cents = num%100;
      num = Math.floor(num/100).toString();
      if(cents<10)
      cents = "0" + cents;
      for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
      num = num.substring(0,num.length-(4*i+3))+','+
      num.substring(num.length-(4*i+3));
      if(sign=='-')
    	  {return "<font color='red'>￥"+(((sign)?'':'-')  + num + '.' + cents)+"</font>";}    	 
      else
    	  {
    	  
    	  if(num=="0" && cents=="00")
    		  {
    		  return "<font color='#cccccc'>￥"+(((sign)?'':'-')  + num + '.' + cents)+"</font>";
    		  }
    	  else
    		  {
    		  return "<font color='#336633'>￥"+(((sign)?'':'-')  + num + '.' + cents)+"</font>";
    		  }
    	  
    	  }    
      
  }
  /** 还原货币格式化函数 **/
  function restoreFormatCurrency(num){
         var num1=num.replace(',','').replace(/,/g,''); 
      return num1;
  }
  
//	$(function(){
//		$("input,select,textarea").bind('change',function(){alert("a");});
//		
//	});
