$.namespace("systemManage.resource.resourceList");
systemManage.resource.resourceList=function(){
	
	//查询参数
	var queryParam={};
	
	/**
	* 查询列表
	*/
	$('#resource-datagrid').datagrid({
		url:ctxPath+'/systemManage/resource/findResourceList',
		loadFilter:pagerFilter,
		method:'post',
		rownumbers:true,
		singleSelect:false,
		pageSize:15, 
		pageList:pageArray,
		pagination:true,
		multiSort:false,
		fitColumns:true,
		fit:true,
		loadMsg:"正在努力加载...",
		columns:[[
			{ field:'checkbox',checkbox:true},
			{ field:'operate',title:'操作',width:100,formatter:updateOperateFormatter},
			{ field:'resourceName',title:'菜单名称',width:100,sortable:true},
			{ field:'url',title:'链接路径',width:150,sortable:true},
			{ field:'enabled',title:'是否可用',width:100,sortable:true,formatter:enabledFormatter},
			{ field:'creatorName',title:'创建人',width:100,sortable:true},
			{ field:'creatorTrueName',title:'创建人名称',width:100,sortable:true},
			{ field:'createDate',title:'创建日期',width:180,sortable:true},
			{ field:'id',hidden:true}
			
			
		]],
		rowStyler: function(index,row){
			if (index%2==0){
				return 'background-color:#F4F4F4';
			}
		},
		onBeforeLoad:function(param){
			$.extend(param,queryParam);
		},
		onLoadSuccess:function(data){
			if(data.total==0){
				$(this).datagridTipInfo('noData');
			}else{
				$('.editcls').linkbutton({plain:true,iconCls:'icon-edit'});  
			}
			
		},
		onLoadError:function(){
			$(this).datagridTipInfo('error');
		}

	}); 
	
	
	/**
	 * 显示修改按钮
	 */
	function updateOperateFormatter(value,row,index){
		 var id=row.id;
		 var btn = '<a class="editcls" title="修改" onclick="systemManage.resource.resourceList.openUpdate('+id+')" href="javascript:void(0)"></a>';  
         return btn;
	}
	
	/**
	 * 给form中的元素绑定事件
	 */
	function formElementBindEvent(){

		/**
		 * 输入菜单名称后自动生成菜单编码
		 */
		$("#resourceName").textbox('textbox').bind("blur",function(event){
			var resourceName=$("#resourceName").textbox("getValue");
			var code=$("#code").textbox("getValue");
			if(util.isEmptyStr(code)){
				$.getJSON(ctxPath+'/systemManage/resource/generateResourceCode',{"resourceName":resourceName},function(data){
					if(data.returnCode=='success'){
						$('#code').textbox("setValue",data.returnData);
					}
				});
			}
			
		});
	}
	
	/**
	 * 对外的方法
	 */
	 return {
		 
		 /**
		  * 根据条件查询datagrid
		  */
		 queryList:function(){
			 queryParam=$("#resource-queryForm").find(".textbox-value").objParam();
			 $('#resource-datagrid').datagrid('load', queryParam);  
		 },
	 
		 /**
		  * 重置
		  */
		 reset:function(){
			 queryParam={};
			 $('#resource-queryForm').form('reset');
		 },
		 
		 /**
		  * 打开添加窗口
		  */
		 openAdd:function(){
				$('#resourceForm').form('reset').form('disableValidation');
				$('#resource-dialog').dialog({
					closed: false,
					modal:true,
			        title: "添加菜单",
			        buttons: [{
			            text: '确定',
			            iconCls: 'icon-ok',
			            handler: add
			        }, {
			            text: '取消',
			            iconCls: 'icon-cancel',
			            handler: function () {
			                $('#resource-dialog').dialog('close');                    
			            }
			        }]
			    });
				
				formElementBindEvent();
		 },
		 

		/**
		* 打开修改窗口
		*/
		 openUpdate:function(id){
			$('#resourceForm').form('reset').form('disableValidation');
			$.ajax({
				url:ctxPath+'/systemManage/resource/getResourceById',
				data:{'id':id},
				success:function(data){
					if(data.returnCode=='success'){
						//绑定值
						$('#resourceForm').form('load', data.returnData);
					}else{
						$('#resource-dialog').dialog('close');
						$.messager.alert('信息提示','没有找到该菜单信息！','info');
					}
				}	
			});
			
			$('#resource-dialog').dialog({
				closed: false,
				modal:true,
		        title: "修改菜单",
		        buttons: [{
		            text: '确定',
		            iconCls: 'icon-ok',
		            handler: update
		        }, {
		            text: '取消',
		            iconCls: 'icon-cancel',
		            handler: function () {
		                $('#resource-dialog').dialog('close');                    
		            }
		        }]
		    });
			
			formElementBindEvent();
			
		},
		
		
		/**
		* 删除记录
		*/
		remove:function(){
			$.messager.confirm('信息提示','确定要删除该记录？', function(result){
				if(result){
					var items = $('#resource-datagrid').datagrid('getSelections');
					if(items.length>0){
						
						$.ajax({  
						    type: "POST",  
						    url: ctxPath+'/systemManage/resource/removeResource',  
						    data: JSON.stringify(items), 
						    dataType:"json",  
						    contentType : 'application/json;charset=utf-8',
						    success: function(data){  
						    	if(data.returnCode=='success'){
						        	$.messager.alert('信息提示','提交成功！','info');
						        	$('#resource-datagrid').datagrid('reload');  
						    	}else if(data.returnCode=='exist_enabled'){
						    		$.messager.alert('信息提示','可用状态的菜单不能删除!','info');
						    		$('#permission-datagrid').datagrid('reload');
						    	}else{
						    		$.messager.alert('信息提示','删除失败！','info');
						    	}
						    },  
						    error: function(res){  
						    	$.messager.alert('信息提示','删除失败！','info');
						    }  
						}); 
						
					}else{
						$.messager.alert('信息提示','请选择要删除的数据！','info');
					}
					
					
				}	
			});
		}


	 
	 }

	 

	/**
	* 添加记录
	*/
	function add(){
		$('#resourceForm').form({   
		    url:ctxPath+'/systemManage/resource/insertResource',   
		    onSubmit: function(param){  
		    	return $(this).form('enableValidation').form('validate'); 
		    },   
		    success:function(data){
		    	data=JSON.parse(data);
		        if(data.returnCode=='success'){
		        	$.messager.alert('信息提示','提交成功！','info');
					$('#resource-dialog').dialog('close');
					$('#resource-datagrid').datagrid('reload');  
		        }else{
		        	$.messager.alert('信息提示','提交失败！','info');
		        }   
		    }   
		}); 
		$('#resourceForm').submit();
	}
	

	/**
	* 修改记录
	*/
	function update(){
		$('#resourceForm').form({   
		    url:ctxPath+'/systemManage/resource/updateResource',   
		    onSubmit: function(param){  
		    	return $(this).form('enableValidation').form('validate'); 
		    },   
		    success:function(data){ 
		    	data=JSON.parse(data);
		        if(data.returnCode=='success'){
		        	$.messager.alert('信息提示','提交成功！','info');
					$('#resource-dialog').dialog('close');
					$('#resource-datagrid').datagrid('reload');  
		        }else{
		        	$.messager.alert('信息提示','提交失败！','info');
		        }   
		    }   
		}); 
		$('#resourceForm').submit();
	}




	
}();

