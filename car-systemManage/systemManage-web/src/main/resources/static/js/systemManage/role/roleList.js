$.namespace("systemManage.role.roleList");
systemManage.role.roleList=function(){
	
	//查询参数
	var queryParam={};
	
	/**
	* 查询列表
	*/
	$('#role-datagrid').datagrid({
		url:ctxPath+'/systemManage/role/findRoleList',
		loadFilter:pagerFilter,
		method:'post',
		rownumbers:true,
		singleSelect:false,
		pageSize:15, 
		pageList:pageArray,
		pagination:true,
		multiSort:false,
		fitColumns:true,
		fit:true,
		loadMsg:"正在努力加载...",
		columns:[[
			{ field:'checkbox',checkbox:true},
			{ field:'operate',title:'操作',width:100,formatter:updateOperateFormatter},
			{ field:'roleName',title:'角色名称',width:100,sortable:true},
			{ field:'enabled',title:'是否可用',width:100,sortable:true,formatter:enabledFormatter},
			{ field:'creatorName',title:'创建人',width:100,sortable:true},
			{ field:'creatorTrueName',title:'创建人名称',width:100,sortable:true},
			{ field:'createDate',title:'创建日期',width:180,sortable:true},
			{ field:'id',hidden:true}
			
			
		]],
		rowStyler: function(index,row){
			if (index%2==0){
				return 'background-color:#F4F4F4';
			}
		},
		onBeforeLoad:function(param){
			$.extend(param,queryParam);
		},
		onLoadSuccess:function(data){
			if(data.total==0){
				$(this).datagridTipInfo('noData');
			}else{
				$('.editcls').linkbutton({plain:true,iconCls:'icon-edit'});  
			}
			
		},
		onLoadError:function(){
			$(this).datagridTipInfo('error');
		}

	}); 
	
	/**
	 * 显示修改按钮
	 */
	function updateOperateFormatter(value,row,index){
		 var id=row.id;
		 var btn = '<a class="editcls" title="修改" onclick="systemManage.role.roleList.openUpdate('+id+')" href="javascript:void(0)"></a>';  
         return btn;
	}
	
	/**
	 * 对外的方法
	 */
	 return {
		 
		 /**
		  * 根据条件查询datagrid
		  */
		 queryList:function(){
			 queryParam=$("#role-queryForm").find(".textbox-value").objParam();
			 $('#role-datagrid').datagrid('load', queryParam);  
		 },
	 
		 /**
		  * 重置
		  */
		 reset:function(){
			 queryParam={};
			 $('#role-queryForm').form('reset');
		 },
		 
		 /**
		  * 打开添加窗口
		  */
		 openAdd:function(){
				$('#roleForm').form('reset').form('disableValidation');
				$('#role-dialog').dialog({
					closed: false,
					modal:true,
			        title: "添加角色",
			        buttons: [{
			            text: '确定',
			            iconCls: 'icon-ok',
			            handler: add
			        }, {
			            text: '取消',
			            iconCls: 'icon-cancel',
			            handler: function () {
			                $('#role-dialog').dialog('close');                    
			            }
			        }]
			    });
		 },
		 

		/**
		* 打开修改窗口
		*/
		 openUpdate:function(id){
			$('#roleForm').form('reset').form('disableValidation');
			$.ajax({
				url:ctxPath+'/systemManage/role/getRoleById',
				data:{'id':id},
				success:function(data){
					if(data.returnCode=='success'){
						//绑定值
						$('#roleForm').form('load', data.returnData);
					}else{
						$('#role-dialog').dialog('close');
						$.messager.alert('信息提示','没有找到该角色信息！','info');
					}
				}	
			});
			
			$('#role-dialog').dialog({
				closed: false,
				modal:true,
		        title: "修改角色",
		        buttons: [{
		            text: '确定',
		            iconCls: 'icon-ok',
		            handler: update
		        }, {
		            text: '取消',
		            iconCls: 'icon-cancel',
		            handler: function () {
		                $('#role-dialog').dialog('close');                    
		            }
		        }]
		    });
		},
		
		
		/**
		* 删除记录
		*/
		remove:function(){
			$.messager.confirm('信息提示','确定要删除该记录？', function(result){
				if(result){
					var items = $('#role-datagrid').datagrid('getSelections');
					if(items.length>0){
						
						$.ajax({  
						    type: "POST",  
						    url: ctxPath+'/systemManage/role/removeRole',  
						    data: JSON.stringify(items), 
						    dataType:"json",  
						    contentType : 'application/json;charset=utf-8',
						    success: function(data){  
						    	if(data.returnCode=='success'){
						        	$.messager.alert('信息提示','提交成功！','info');
						        	$('#role-datagrid').datagrid('reload');  
						    	}else if(data.returnCode=='exist_enabled'){
						    		$.messager.alert('信息提示','可用状态的角色不能删除!','info');
						    		$('#permission-datagrid').datagrid('reload');
						    	}else{
						    		$.messager.alert('信息提示','删除失败！','info');
						    	}
						    },  
						    error: function(res){  
						    	$.messager.alert('信息提示','删除失败！','info');
						    }  
						}); 
						
					}else{
						$.messager.alert('信息提示','请选择要删除的数据！','info');
					}
					
					
				}	
			});
		}


	 
	 }

	 

	/**
	* 添加记录
	*/
	function add(){
		$('#roleForm').form({   
		    url:ctxPath+'/systemManage/role/insertRole',   
		    onSubmit: function(param){  
		    	return $(this).form('enableValidation').form('validate'); 
		    },   
		    success:function(data){
		    	data=JSON.parse(data);
		        if(data.returnCode=='success'){
		        	$.messager.alert('信息提示','提交成功！','info');
					$('#role-dialog').dialog('close');
					$('#role-datagrid').datagrid('reload');  
		        }else{
		        	$.messager.alert('信息提示','提交失败！','info');
		        }   
		    }   
		}); 
		$('#roleForm').submit();
	}
	

	/**
	* 修改记录
	*/
	function update(){
		$('#roleForm').form({   
		    url:ctxPath+'/systemManage/role/updateRole',   
		    onSubmit: function(param){  
		    	return $(this).form('enableValidation').form('validate'); 
		    },   
		    success:function(data){ 
		    	data=JSON.parse(data);
		        if(data.returnCode=='success'){
		        	$.messager.alert('信息提示','提交成功！','info');
					$('#role-dialog').dialog('close');
					$('#role-datagrid').datagrid('reload');  
		        }else{
		        	$.messager.alert('信息提示','提交失败！','info');
		        }   
		    }   
		}); 
		$('#roleForm').submit();
	}




	
}();

