$.namespace("systemManage.permission.permissionList");
systemManage.permission.permissionList=function(){
	
	//查询参数
	var queryParam={};
	
	/**
	* 查询列表
	*/
	$('#permission-datagrid').datagrid({
		url:ctxPath+'/systemManage/permission/findPermissionList',
		loadFilter:pagerFilter,
		method:'post',
		rownumbers:true,
		singleSelect:false,
		pageSize:15, 
		pageList:pageArray,
		pagination:true,
		multiSort:false,
		fitColumns:true,
		fit:true,
		loadMsg:"正在努力加载...",
		columns:[[
			{ field:'checkbox',checkbox:true},
			{ field:'operate',title:'操作',width:100,formatter:updateOperateFormatter},
			{ field:'permissionName',title:'权限名称',width:100,sortable:true},
			{ field:'code',title:'权限编码',width:100,sortable:true},
			{ field:'enabled',title:'是否可用',width:100,sortable:true,formatter:enabledFormatter},
			{ field:'creatorName',title:'创建人',width:100,sortable:true},
			{ field:'creatorTrueName',title:'创建人名称',width:100,sortable:true},
			{ field:'createDate',title:'创建日期',width:180,sortable:true},
			{ field:'id',hidden:true}
			
			
		]],
		rowStyler: function(index,row){
			if (index%2==0){
				return 'background-color:#F4F4F4';
			}
		},
		onBeforeLoad:function(param){
			$.extend(param,queryParam);
		},
		onLoadSuccess:function(data){
			if(data.total==0){
				$(this).datagridTipInfo('noData');
			}else{
				$('.editcls').linkbutton({plain:true,iconCls:'icon-edit'});  
			}
			
		},
		onLoadError:function(){
			$(this).datagridTipInfo('error');
		}

	}); 
	
	/**
	 * 显示修改按钮
	 */
	function updateOperateFormatter(value,row,index){
		 var id=row.id;
		 var btn = '<a class="editcls" title="修改" onclick="systemManage.permission.permissionList.openUpdate('+id+')" href="javascript:void(0)"></a>';  
         return btn;
	}
	
	/**
	 * 对外的方法
	 */
	 return {
		 
		 /**
		  * 根据条件查询datagrid
		  */
		 queryList:function(){
			 queryParam=$("#permission-queryForm").find(".textbox-value").objParam();
			 $('#permission-datagrid').datagrid('load', queryParam);  
		 },
	 
		 /**
		  * 重置
		  */
		 reset:function(){
			 queryParam={};
			 $('#permission-queryForm').form('reset');
		 },
		 
		 /**
		  * 打开添加窗口
		  */
		 openAdd:function(){
				$('#permissionAddForm').form('reset').form('disableValidation');
				$('#permission-add-dialog').dialog({
					closed: false,
					modal:true,
			        title: "添加权限",
			        buttons: [{
			            text: '确定',
			            iconCls: 'icon-ok',
			            handler: add
			        }, {
			            text: '取消',
			            iconCls: 'icon-cancel',
			            handler: function () {
			                $('#permission-add-dialog').dialog('close');                    
			            }
			        }]
			    });
		 },
		 

		/**
		* 打开修改窗口
		*/
		 openUpdate:function(id){
			$('#permissionUpdateForm').form('reset').form('disableValidation');
			$.ajax({
				url:ctxPath+'/systemManage/permission/getPermissionById',
				data:{'id':id},
				success:function(data){
					if(data.returnCode=='success'){
						//绑定值
						$('#permissionUpdateForm').form('load', data.returnData);
					}else{
						$('#permission-update-dialog').dialog('close');
						$.messager.alert('信息提示','没有找到该权限信息！','info');
					}
				}	
			});
			
			$('#permission-update-dialog').dialog({
				closed: false,
				modal:true,
		        title: "修改权限",
		        buttons: [{
		            text: '确定',
		            iconCls: 'icon-ok',
		            handler: update
		        }, {
		            text: '取消',
		            iconCls: 'icon-cancel',
		            handler: function () {
		                $('#permission-update-dialog').dialog('close');                    
		            }
		        }]
		    });
		},
		
		
		/**
		* 删除记录
		*/
		remove:function(){
			$.messager.confirm('信息提示','确定要删除该记录？', function(result){
				if(result){
					var items = $('#permission-datagrid').datagrid('getSelections');
					if(items.length>0){
						
						$.ajax({  
						    type: "POST",  
						    url: ctxPath+'/systemManage/permission/removePermission',  
						    data: JSON.stringify(items), 
						    dataType:"json",  
						    contentType : 'application/json;charset=utf-8',
						    success: function(data){  
						    	if(data.returnCode=='success'){
						        	$.messager.alert('信息提示','提交成功！','info');
						        	$('#permission-datagrid').datagrid('reload');  
						    	}else if(data.returnCode=='exist_enabled'){
						    		$.messager.alert('信息提示','可用状态的权限不能删除!','info');
						    		$('#permission-datagrid').datagrid('reload');
						    	}else{
						    		$.messager.alert('信息提示','删除失败！','info');
						    	}
						    },  
						    error: function(res){  
						    	$.messager.alert('信息提示','删除失败！','info');
						    }  
						}); 
						
					}else{
						$.messager.alert('信息提示','请选择要删除的数据！','info');
					}
					
					
				}	
			});
		}


	 
	 }

	 

	/**
	* 添加记录
	*/
	function add(){
		$('#permissionAddForm').form({   
		    url:ctxPath+'/systemManage/permission/insertPermission',   
		    onSubmit: function(param){  
		    	return $(this).form('enableValidation').form('validate'); 
		    },   
		    success:function(data){
		    	data=JSON.parse(data);
		        if(data.returnCode=='success'){
		        	$.messager.alert('信息提示','提交成功！','info');
					$('#permission-add-dialog').dialog('close');
					$('#permission-datagrid').datagrid('reload');  
		        }else{
		        	$.messager.alert('信息提示','提交失败！','info');
		        }   
		    }   
		}); 
		$('#permissionAddForm').submit();
	}
	

	/**
	* 修改记录
	*/
	function update(){
		$('#permissionUpdateForm').form({   
		    url:ctxPath+'/systemManage/permission/updatePermission',   
		    onSubmit: function(param){  
		    	return $(this).form('enableValidation').form('validate'); 
		    },   
		    success:function(data){ 
		    	data=JSON.parse(data);
		        if(data.returnCode=='success'){
		        	$.messager.alert('信息提示','提交成功！','info');
					$('#permission-update-dialog').dialog('close');
					$('#permission-datagrid').datagrid('reload');  
		        }else{
		        	$.messager.alert('信息提示','提交失败！','info');
		        }   
		    }   
		}); 
		$('#permissionUpdateForm').submit();
	}




	
}();

