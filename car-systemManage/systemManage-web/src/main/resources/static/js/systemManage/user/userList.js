console.log('使用commonUtil:'+util.isEmptyStr("fff"));
console.log('使用上下文路径:'+ctxPath);

$.namespace("systemManage.user.userList");
systemManage.user.userList=function(){
	
	//查询参数
	var queryParam={};
	
	/**
	* 查询列表
	*/
	$('#user-datagrid').datagrid({
		url:ctxPath+'/systemManage/user/findUserList',
		loadFilter:pagerFilter,
		method:'post',
		rownumbers:true,
		singleSelect:false,
		pageSize:15, 
		pageList:pageArray,
		pagination:true,
		multiSort:false,
		fitColumns:true,
		fit:true,
		loadMsg:"正在努力加载...",
		columns:[[
			{ field:'checkbox',checkbox:true},
			{ field:'operate',title:'操作',width:100,formatter:updateOperateFormatter},
			{ field:'userName',title:'用户名',width:100,sortable:true},
			{ field:'trueName',title:'姓名',width:100,sortable:true},
			{ field:'enabled',title:'是否可用',width:100,sortable:true,formatter:enabledFormatter},
			{ field:'creatorName',title:'创建人',width:100,sortable:true},
			{ field:'creatorTrueName',title:'创建人名称',width:100,sortable:true},
			{ field:'createDate',title:'创建日期',width:180,sortable:true},
			{ field:'id',hidden:true}
			
			
		]],
		rowStyler: function(index,row){
			if (index%2==0){
				return 'background-color:#F4F4F4';
			}
		},
		onBeforeLoad:function(param){
			$.extend(param,queryParam);
		},
		onLoadSuccess:function(data){
			if(data.total==0){
				$(this).datagridTipInfo('noData');
			}else{
				$('.editcls').linkbutton({plain:true,iconCls:'icon-edit'});  
			}
			
		},
		onLoadError:function(){
			$(this).datagridTipInfo('error');
		}

	}); 
	
	/**
	 * 格式化显示修改按钮
	 */
	function updateOperateFormatter(value,row,index){
		 var id=row.id;
		 var btn = '<a class="editcls" title="修改" onclick="systemManage.user.userList.openUpdate('+id+')" href="javascript:void(0)"></a>';  
         return btn;
	}
	

	/**
	 * 对外的方法
	 */
	 return {
		 
		 /**
		  * 根据条件查询datagrid
		  */
		 queryList:function(){
			 queryParam=$("#user-queryForm").find(".textbox-value").objParam();
			 $('#user-datagrid').datagrid('load', queryParam);  
		 },
	 
		 /**
		  * 重置
		  */
		 reset:function(){
			 queryParam={};
			 $('#user-queryForm').form('reset');
		 },
		 
		 /**
		  * 打开添加窗口
		  */
		 openAdd:function(){
				$('#userAddForm').form('reset').form('disableValidation');
				$('#user-add-dialog').dialog({
					closed: false,
					modal:true,
			        title: "添加用户",
			        buttons: [{
			            text: '确定',
			            iconCls: 'icon-ok',
			            handler: add
			        }, {
			            text: '取消',
			            iconCls: 'icon-cancel',
			            handler: function () {
			                $('#user-add-dialog').dialog('close');                    
			            }
			        }]
			    });
		 },
		 

		/**
		* 打开修改窗口
		*/
		 openUpdate:function(id){
			$('#userUpdateForm').form('reset').form('disableValidation');
			$.ajax({
				url:ctxPath+'/systemManage/user/getUserById',
				data:{'id':id},
				success:function(data){
					if(data.returnCode=='success'){
						//绑定值
						$('#userUpdateForm').form('load', data.returnData);
					}else{
						$('#user-update-dialog').dialog('close');
						$.messager.alert('信息提示','没有找到该用户信息！','info');
					}
				}	
			});
			
			$('#user-update-dialog').dialog({
				closed: false,
				modal:true,
		        title: "修改用户",
		        buttons: [{
		            text: '确定',
		            iconCls: 'icon-ok',
		            handler: update
		        }, {
		            text: '取消',
		            iconCls: 'icon-cancel',
		            handler: function () {
		                $('#user-update-dialog').dialog('close');                    
		            }
		        }]
		    });
		},
		
		
		/**
		* 删除记录
		*/
		remove:function(){
			$.messager.confirm('信息提示','确定要删除该记录？', function(result){
				if(result){
					var items = $('#user-datagrid').datagrid('getSelections');
					if(items.length>0){
						
						$.ajax({  
						    type: "POST",  
						    url: ctxPath+'/systemManage/user/removeUser',  
						    data: JSON.stringify(items), 
						    dataType:"json",  
						    contentType : 'application/json;charset=utf-8',
						    success: function(data){  
						    	if(data.returnCode=='success'){
						        	$.messager.alert('信息提示','提交成功！','info');
						        	$('#user-datagrid').datagrid('reload');  
						    	}else if(data.returnCode=='exist_enabled'){
						    		$.messager.alert('信息提示','可用状态的用户不能删除!','info');
						    		$('#permission-datagrid').datagrid('reload');
						    	}else{
						    		$.messager.alert('信息提示','删除失败！','info');
						    	}
						    },  
						    error: function(res){  
						    	$.messager.alert('信息提示','删除失败！','info');
						    }  
						}); 
						
					}else{
						$.messager.alert('信息提示','请选择要删除的数据！','info');
					}
					
					
				}	
			});
		}


	 
 }

	 

	/**
	* 添加记录
	*/
	function add(){
		$('#userAddForm').form({   
		    url:ctxPath+'/systemManage/user/insertUser',   
		    onSubmit: function(param){  
		    	return $(this).form('enableValidation').form('validate'); 
		    },   
		    success:function(data){
		    	data=JSON.parse(data);
		        if(data.returnCode=='success'){
		        	$.messager.alert('信息提示','提交成功！','info');
					$('#user-add-dialog').dialog('close');
					$('#user-datagrid').datagrid('reload');  
		        }else{
		        	$.messager.alert('信息提示','提交失败！','info');
		        }   
		    }   
		}); 
		$('#userAddForm').submit();
	}
	

	/**
	* 修改记录
	*/
	function update(){
		$('#userUpdateForm').form({   
		    url:ctxPath+'/systemManage/user/updateUser',   
		    onSubmit: function(param){  
		    	return $(this).form('enableValidation').form('validate'); 
		    },   
		    success:function(data){ 
		    	data=JSON.parse(data);
		        if(data.returnCode=='success'){
		        	$.messager.alert('信息提示','提交成功！','info');
					$('#user-update-dialog').dialog('close');
					$('#user-datagrid').datagrid('reload');  
		        }else{
		        	$.messager.alert('信息提示','提交失败！','info');
		        }   
		    }   
		}); 
		$('#userUpdateForm').submit();
	}




	
}();

