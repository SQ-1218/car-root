(function(global){

	//使用严格模式确保代码准确度
    "use strict";
	
     var  commonUtil = function(opts){
        this.options = {
                version: 'v 1.0',
		}

        if(opts) {
          for(var key in opts) {
            if(!!opts[key]){
                this.options[key] = opts[key];
            }

          }
        }

     }


     commonUtil.prototype = {
    	constructor: commonUtil,
		getVersion: function(){
		    return this.options.version;
		},
     
     	/**
     	 * 判断变量是否为null,undefined,"";
     	 * 是返回true，否返回false
     	 */
     	isEmptyStr: function(text){
     		 if(text==null){
     		    return true;
     		 }else{
     		     var tmp=(text+"").replace(/(^\s*)|(\s*$)/g,"");
     		     if(!tmp){
     		    	 return true;
     		     }
     		     return false;
     		 }
     	},
     	
     	/**
     	 * 四舍五入
     	 * v 数值
     	 * e 保留小数点位数
     	 * 例如：rounddigits(12.5656565,2)
     	 */
     	roundDigits: function(v,e){
     		var t=1;
     		for(;e>0;t*=10,e--);
     		for(;e<0;t/=10,e++);
     		return Math.round(v*t)/t;
     	},
		
		/**
		 * 格式化数值
		 * num表示要被格式化的数值
		 * scale表示四舍五入后保留小数点的位数
		 */
		formatNumber: function(num,scale){
			var add = 0;
			var s,temp;
			var s1 = num + "";
			var start = s1.indexOf(".");
			if(start>=0 && s1.substr(start+len+1,1)>=5)add=1;
			var temp = Math.pow(10,scale);
			s = Math.floor(num * temp) + add;
			return s/temp;
		},
     	
     	/**
     	 * 回车调用指定函数
     	 * evt表示回车事件event
     	 * paramfunc表示要调用的函数
     	 */
     	enterIn: function(evt,paramfunc){
     		//兼容IE和FF
   		  var evt=evt?evt:(window.event?window.event:null);
	 		if (evt.keyCode==13){
	 			paramfunc.call(this);
	 		}
     	}
     	
     	

    }

    global.commonUtil = commonUtil;

}(this))

/**
 * js工具类插件的定义
 */
var util=new commonUtil();

/**
 * 命名空间的定义
 */
$.namespace = function() {
    var a=arguments, o=null, i, j, d;
    for (i=0; i<a.length; i=i+1) {
        d=a[i].split(".");
        o=window;
        for (j=0; j<d.length; j=j+1) {
            o[d[j]]=o[d[j]] || {};
            o=o[d[j]];
        }
    }
    return o;
};


/*******************下面是页面的公用方法***************************/

/**
 * 获得form表单中的元素的值。
 */
jQuery.fn.objParam=function(){
	this.find(":focus").blur();
	var data={};
	if(this.length>0){
	var fields =this.serializeArray();
	jQuery.each( fields, function(i, field){
		var key=field.name;
		var keyvalue=$.trim(field.value);
		keyvalue=keyvalue.replace(/"/g,"&#34;");
		keyvalue=keyvalue.replace(/'/g,"&#39;");
		if(!util.isEmptyStr(keyvalue) && !util.isEmptyStr(key)){
			if(data[key]==undefined)
				data[key]=keyvalue;
			else
				data[key]=data[key]+","+keyvalue;	
		}
	});
	}
	return data;
};


/**
 * 格式化显示是否可用
 */
function enabledFormatter(value,row,index){
	 if(value=='N'){
		 return '不可用';
	 }  
    return "可用";
}


/**
* easyui datagrid的loadFilter过滤数据，将会在加载数据时执行,必须返回{'total':num,'rows':[{},...]}格式的数据
* resultData是后台返回的数据。
*/
function pagerFilter(resultData){ 
	var data = {                   
		total: resultData.total,                   
		rows: resultData.list               
	}       

	var dg = $(this);         
	var opts = dg.datagrid('options');          
	var pager = dg.datagrid('getPager');          
	pager.pagination({                
		onSelectPage:function(pageNum, pageSize){    
			opts.pageNumber = pageNum;                   
			opts.pageSize = pageSize;                
			pager.pagination('refresh',{pageNumber:pageNum,pageSize:pageSize});                  
			dg.datagrid('reload',{});
		}          
	});           
	      
	return data;      
}

/**
 * 当无数据或者加载出错时，datagrid的提示信息
 * @returns
 */
jQuery.fn.datagridTipInfo=function(type){
	var info='';
	if(type=="error"){
		info='加载数据出错！';
	}else if(type=="noData"){
		info='暂无数据！';
	}

	$.messager.show({
		title:'信息提示',
		msg:info,
		timeout:1000,
		showType:'slide'
	});

};



/**
 * easyui自定义验证
 */
$.extend($.fn.validatebox.defaults.rules, {
	//验证多个规则的写法：validType:['email','length[0,20]'
	/**
	 *  easyUI已经提供的验证validType规则
	 *  email：匹配 email 正则表达式规则。
	 *  url：匹配 URL 正则表达式规则。
	 *  length[0,100]：允许从 x 到 y 个字符。
	 *  remote['http://.../action.do','paramName']：发送 ajax 请求来验证值，成功时返回 'true' 
	 */
	notEmpty: { 
				/**
				 * 非空字符串验证。 easyui 原装required 不能验证空格。
				 * 在使用notEmpty时，必须先required: true,因为只有required为true在不进行输入点击提交时才进行验证。
				 * 例如：data-options="label:'登录名称:',required: true,validType:['notEmpty','length[0,20]']"
				 * @returns
				 */
				validator : function(value, param) {
					return $.trim(value).length>0;
				}, 
				message : '该输入项为必输项'
			},
			
	 positiveInteger: {// 验证正整数不包括0
				validator : function(value) {
					return /^[0-9]*[1-9][0-9]*$/i.test(value);
				},
				message : '请输入正整数'
			},
			
	 integer: {// 验证整数
				validator : function(value) {
					return /^-?\d+$/i.test(value);
				},
				message : '请输入整数'
			},
			
	 float: {// 验证浮点数
				validator : function(value) {
					return /^(-?\d+)(\.\d+)?$/i.test(value);
				},
				message : '请输入浮点数'
			},	
			
	 positiveFloat: {// 验证非负浮点数(非负浮点数 + 0)
			validator : function(value) {
				return /^(-?\d+)(\.\d+)?$/i.test(value);
			},
			message : '请输入非负浮点数'
		},	
		
	 number: { // 包括正整数与小数
			validator : function(value, param) {
				return /^\d+(\.\d+)?$/.test(value);
			},
			message : '请输入数字'
		},		
		
			　
	 english: {// 验证字母
				validator : function(value) {
					return /^[A-Za-z]+$/i.test(value);
				},
				message : '请输入字母'
			},
			
	 carNo: {//验证车牌
				validator : function(value) {
				return /^[\u4E00-\u9FA5][\da-zA-Z]{6}$/.test(value);
				},
				message : '车牌号码无效（例：粤B12350）'
			},
			
	 account: {// param的值为[]中值,用户账号验证(只能包括 _ 数字 字母) 
				validator: function (value, param) 
				{
					if (value.length < param[0] || value.length > param[1]) 
					{
						$.fn.validatebox.defaults.rules.account.message = '用户名长度必须在' + param[0] + '至' + param[1] + '范围';
						return false;
					} 
					else 
					{
						if (!/^[\w]+$/.test(value)) 
						{
							$.fn.validatebox.defaults.rules.account.message = '用户名只能数字、字母、下划线组成.';
							return false;
						} 
						else 
						{
							return true;
						}
					}
				},
				message: ''
				},
			
	 minLength: { //例如：validType:'minLength[3]'
			validator: function(value, param){
				return value.length >= param[0];
			},
			message: '请输入至少{0}个字符.'
	    },	

			
	date: {// 验证日期
		validator : function(value) {
			// 格式yyyy-MM-dd或yyyy-M-d
			return /^(?:(?!0000)[0-9]{4}([-]?)(?:(?:0?[1-9]|1[0-2])\1(?:0?[1-9]|1[0-9]|2[0-8])|(?:0?[13-9]|1[0-2])\1(?:29|30)|(?:0?[13578]|1[02])\1(?:31))|(?:[0-9]{2}(?:0[48]|[2468][048]|[13579][26])|(?:0[48]|[2468][048]|[13579][26])00)([-]?)0?2\2(?:29))$/i
					.test(value);
		},
		message : '清输入合适的日期格式'
	},		

	chs: {
		validator: function (value) {
		return /^[\u0391-\uFFE5]+$/.test(value);
		},
		message: '只能输入汉字'
	},
		
	idcard: {// 验证身份证
		validator : function(value) {
			return /^\d{15}(\d{2}[A-Za-z0-9])?$/i.test(value);
		},
		message : '身份证号码格式不正确'
	},

	equals: { // 验证密码两次的输入是否相同,例如： data-options="validType:['notEmpty','length[6,20]','equals[\'password\']']"
			validator : function(value, param) {
				if ($("#" + param[0]).val() != "" && value != "") {
					return $("#" + param[0]).val() == value;
				} else {
					return true;
				}
			},
			message : '两次输入的密码不一致！'
		}	
});
