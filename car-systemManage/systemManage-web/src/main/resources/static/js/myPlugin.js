(function ($) {
	
	//默认参数
	var defaluts = {
	    version: 'v 1.0',
	  };
	 
    function MyPlugin($ele, options) {
        this.$ele = $ele;
        this.options = options;
        this.init();
    }

    MyPlugin.prototype = {
        constructor: MyPlugin,
        init: function () {
        	//初始化方法
          console.log(this.options); 
        },
        getVersion: function(){
        console.log($.fn.myPlugin.outterFunction("plugin template"));	
        console.log(innerFunction());
	    return this.options.version;
        }	
   };

  $.fn.myPlugin= function (options) {
      var opts = $.extend({}, defaluts, options); 
      return new MyPlugin($(this), options);
    };

  $.fn.myPlugin.outterFunction = function (str) {
    return "<strong>" + str + "</strong>";
  };

  function innerFunction() {
    return "innerFunction";
  }
  
  /*****************jQuery插件编写****************************/
  
    /**
     * 回车触发change事件
     */
  	jQuery.fn.enterKeyChange=function(){
		this.bind("keypress",function(event){
			var keyCode=event.keyCode?event.keyCode:event.which?event.which:event.charCode;
			//回车事件触发change
			if(keyCode==13)
				$(this).trigger("change");
		});
	};
	
	/**
	 * 得到form中元素的数据，返回的是JSON格式的对象
	 */
	jQuery.fn.formData=function(){
		this.find(":focus").blur();
		var data={};
		if(this.length>0){
		var fields =this.serializeArray();
		jQuery.each( fields, function(i, field){
			var key=field.name;
			var keyvalue=$.trim(field.value);
			if(!$.isEmptyStr(keyvalue) && !$.isEmptyStr(key)){
				if(data[key]==undefined)
					data[key]=keyvalue;
				else
					data[key]=data[key]+","+keyvalue;	
			}
		});
		}
		return data;
	};
	
	/**
	 * 将JSON对象按照名称填充到form中的元素中
	 */
	jQuery.fn.fillForm=function(fmdata){
		var formElement=this;
		if(jQuery.isPlainObject(fmdata)){
			jQuery.each(fmdata,function(key,value){
				var element=formElement.find("input[name='"+key+"']");
				if(element.length==1){
					element.val(value);
				}else{
					$.each(elment,function(i){
						var inputType=$(".test").attr("type");
						if(inputType=='checkbox'||inputType=='radio'){
							if($(this).val()==value)
							$(this).prop("checked",true);
						}
					});
					
				}
			});
		}
	};
	
	

})(jQuery);
