package com.magicrule.car.systemManage;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ImportResource;

import com.spring4all.mongodb.EnableMongoPlus;

//配置过滤器，拦截器需要的注解
@EnableMongoPlus
@ServletComponentScan
@ImportResource(locations = { "classpath:config/applicationContext.xml" }) 
@ComponentScan(basePackages = { "com.magicrule.car.systemManage" })
@MapperScan("com.magicrule.car.systemManage.dao.api")
@SpringBootApplication
public class StartApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(StartApplication.class, args);
    }

}
