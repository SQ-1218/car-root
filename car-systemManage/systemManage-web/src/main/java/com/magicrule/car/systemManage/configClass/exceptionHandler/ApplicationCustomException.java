package com.magicrule.car.systemManage.configClass.exceptionHandler;

public class ApplicationCustomException extends Exception {

	private static final long serialVersionUID = 1L;  
	private String message="";
	
	public ApplicationCustomException(String description){
		super(description);
		/**
		 * 日志记录
		 * log.error(description);
		 */
	}
	
	public ApplicationCustomException(String description,Exception e) {
		super(description);
		this.message=description;
		String exceptionMessage=getExceptionMessage(e);
		/**
		 * 日志记录
		 * log.error(exceptionMessage);
		 */
	}
	
	public ApplicationCustomException(Exception e) {
		super();
		String exceptionMessage=getExceptionMessage(e);
		/**
		 * 日志记录
		 * log.error(exceptionMessage);
		 */
	}
	
	public String getExceptionMessage(Exception e) {
        StringBuffer bs = new StringBuffer();
        StackTraceElement[] a = e.getStackTrace();
        bs.append("\r\n start异常信息: " + message +"\r\n"+e.fillInStackTrace());
        for (int i = 0; i < a.length; i++) {
            bs.append("\r\n在" + a[i].getClassName() + "类(" + a[i].getFileName() + ":" + a[i].getLineNumber() + "行,"
                    + a[i].getMethodName() + "()方法)");
        }
        return bs.toString();
	}
	
}
