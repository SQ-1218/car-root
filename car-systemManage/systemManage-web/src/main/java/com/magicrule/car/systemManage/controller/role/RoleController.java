package com.magicrule.car.systemManage.controller.role;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.magicrule.car.systemManage.configClass.redis.RedisUtil;
import com.magicrule.car.systemManage.controller.base.BaseController;
import com.magicrule.car.systemManage.model.ResponseVO;
import com.magicrule.car.systemManage.model.Role;
import com.magicrule.car.systemManage.service.api.RoleServiceApi;
import com.magicrule.prj.common.ConfigConsts;
import com.magicrule.prj.common.page.PageParam;

/**
 * 
 * @ClassName:  RoleController   
 * @Description:TODO(角色管理)   
 * @author: hebb
 * @date:   2019年2月11日 下午9:51:10   
 *     
 * @Copyright: 2019 www.magicruler.com Inc. All rights reserved. 
 * 注意：本内容仅限于魔尺信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目的
 */
@Controller
@RequestMapping(value="/systemManage/role")
public class RoleController extends BaseController {

	protected static Logger log = LoggerFactory.getLogger(RoleController.class);
	
	@Autowired
	RoleServiceApi roleServiceApi;
	
	@Autowired
	RedisUtil redisUtil;
	
	/**
	 * 
	 * @Title: roleList   
	 * @Description: TODO(跳转到角色列表操作页面)   
	 * @param: @param user
	 * @param: @return      
	 * @return: String      
	 * @throws
	 */
	@RequestMapping("/roleList")
	public String roleList(Role role){
		return "systemManage/role/roleList";
	}
	
	/**
	 * 
	 * @Title: findRoleList   
	 * @Description: TODO(查询角色列表信息)   
	 * @param: @param role
	 * @param: @return      
	 * @return: List<Role>      
	 * @throws
	 */
	@ResponseBody
	@RequestMapping("/findRoleList")
	public PageInfo<Role> findRoleList(Role role, PageParam pageParam){
		String orderBy=null;
		if(StringUtils.hasText(pageParam.getSort())) {
			orderBy=pageParam.getSort()+" "+pageParam.getOrder();
		}
		
		PageHelper.startPage(pageParam.getPage(),pageParam.getRows(),orderBy);
		List<Role> roleList=roleServiceApi.findRoleList(role);
		PageInfo<Role> pageInfo = new PageInfo<Role>(roleList);
		
	    return pageInfo;
	}
	
	
	/**
	 * 
	 * @Title: getRoleById   
	 * @Description: TODO(根据角色Id查询角色)   
	 * @param: @param id
	 * @param: @return      
	 * @return: Role      
	 * @throws
	 */
	@ResponseBody
	@RequestMapping("/getRoleById")
	public ResponseVO getRoleById(String id){
		ResponseVO responseVO=new ResponseVO();
		responseVO.setReturnCode(ConfigConsts.SUCCESS);
		
		Role role=roleServiceApi.getRoleById(id);
		if(role!=null) {
			responseVO.setReturnData(role);
		}else {
			responseVO.setReturnCode(ConfigConsts.FAIL);
		}
		
		return responseVO;
	}
	
	

	/**
	 * 
	 * @Title: removeRole   
	 * @Description: TODO(删除角色)   
	 * @param: @param roleList
	 * @param: @return      
	 * @return: int      
	 * @throws
	 */
	@ResponseBody
	@RequestMapping("/removeRole")
	public ResponseVO removeRole(@RequestBody List<Role> roleList){
		ResponseVO responseVO=new ResponseVO();
		String result=roleServiceApi.removeRole(roleList);
		responseVO.setReturnCode(result);
		return responseVO;
	}
	
	/**
	 * 
	 * @Title: updateRole   
	 * @Description: TODO(修改角色)   
	 * @param: @param role
	 * @param: @return      
	 * @return: int      
	 * @throws
	 */
	@ResponseBody
	@RequestMapping("/updateRole")
	public ResponseVO updateRole(Role role) throws Exception{
		ResponseVO responseVO=new ResponseVO();
		responseVO.setReturnCode(ConfigConsts.SUCCESS);
		
		setModifierInfo(role);
		
		roleServiceApi.updateRole(role);
		return responseVO;
		
	}
	
	/**
	 * 
	 * @Title: insertRole   
	 * @Description: TODO(新增角色)   
	 * @param: @param role
	 * @param: @return      
	 * @return: int      
	 * @throws
	 */
	@ResponseBody
	@RequestMapping("/insertRole")
	public ResponseVO insertRole(Role role) throws Exception{
		
		ResponseVO responseVO=new ResponseVO();
		responseVO.setReturnCode(ConfigConsts.SUCCESS);
		
		setCreatorInfo(role);
		
		roleServiceApi.insertRole(role);
	    return responseVO;
		
	}
	
	
	
}
