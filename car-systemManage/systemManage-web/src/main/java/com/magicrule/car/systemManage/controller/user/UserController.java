package com.magicrule.car.systemManage.controller.user;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.magicrule.car.systemManage.configClass.redis.RedisUtil;
import com.magicrule.car.systemManage.controller.base.BaseController;
import com.magicrule.car.systemManage.model.ResponseVO;
import com.magicrule.car.systemManage.model.User;
import com.magicrule.car.systemManage.service.api.UserServiceApi;
import com.magicrule.prj.common.ConfigConsts;
import com.magicrule.prj.common.page.PageParam;


/**
 * 
 * @ClassName:  UserController   
 * @Description:TODO(用户管理)   
 * @author: hebb
 * @date:   2018年12月6日 下午10:15:39   
 *     
 * @Copyright: 2018 www.magicruler.com Inc. All rights reserved. 
 * 注意：本内容仅限于魔尺信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目的
 */
@Controller
@RequestMapping(value="/systemManage/user")
public class UserController  extends BaseController {

	protected static Logger log = LoggerFactory.getLogger(UserController.class);  
	
	@Autowired
	UserServiceApi userServiceApi;
	
	@Autowired
	RedisUtil redisUtil;
	
	/**
	 * 
	 * @Title: userList   
	 * @Description: TODO(跳转到用户列表操作页面)   
	 * @param: @param user
	 * @param: @return      
	 * @return: String      
	 * @throws
	 */
	@RequestMapping("/userList")
	public String userList(User user){
		return "systemManage/user/userList";
	}
	
	/**
	 * 
	 * @Title: findUserList   
	 * @Description: TODO(查询用户列表信息)   
	 * @param: @param user
	 * @param: @return      
	 * @return: List<User>      
	 * @throws
	 */
	@ResponseBody
	@RequestMapping("/findUserList")
	public PageInfo<User> findUserList(User user, PageParam pageParam){
		String orderBy=null;
		if(StringUtils.hasText(pageParam.getSort())) {
			orderBy=pageParam.getSort()+" "+pageParam.getOrder();
		}
		
		PageHelper.startPage(pageParam.getPage(),pageParam.getRows(),orderBy);
		List<User> userList=userServiceApi.findUserList(user);
		PageInfo<User> pageInfo = new PageInfo<User>(userList);
		
	    return pageInfo;
	}
	
	/**
	 * 
	 * @Title: getUserById   
	 * @Description: TODO(根据用户Id查找用户)   
	 * @param: @param id
	 * @param: @return      
	 * @return: ResponseVO      
	 * @throws
	 */
	@ResponseBody
	@RequestMapping("/getUserById")
	public ResponseVO getUserById(Long id){
		ResponseVO responseVO=new ResponseVO();
		responseVO.setReturnCode(ConfigConsts.SUCCESS);
		
		User user=userServiceApi.getUserById(id);
		if(user!=null) {
			responseVO.setReturnData(user);
		}else {
			responseVO.setReturnCode(ConfigConsts.FAIL);
		}
		
		return responseVO;
	}

	/**
	 * 
	 * @Title: insertUser   
	 * @Description: TODO(新增用户)   
	 * @param: @param user
	 * @param: @return      
	 * @return: ResponseVO      
	 * @throws
	 */
	@ResponseBody
	@RequestMapping("/insertUser")
	public ResponseVO insertUser(User user) throws Exception{
		ResponseVO responseVO=new ResponseVO();
		responseVO.setReturnCode(ConfigConsts.SUCCESS);
		
		encryptPassword(user);
		setCreatorInfo(user);
		
		userServiceApi.insertUser(user);
	    return responseVO;
	}
	
	/**
	 * 
	 * @Title: updateUser   
	 * @Description: TODO(修改用户)   
	 * @param: @param user
	 * @param: @return      
	 * @return: ResponseVO      
	 * @throws
	 */
	@ResponseBody
	@RequestMapping("/updateUser")
	public ResponseVO updateUser(User user) throws Exception{
		ResponseVO responseVO=new ResponseVO();
		responseVO.setReturnCode(ConfigConsts.SUCCESS);
		
		setModifierInfo(user);
		userServiceApi.updateUser(user);
		return responseVO;
		
	}
	
	/**
	 * 
	 * @Title: removeUser   
	 * @Description: TODO(删除用户)   
	 * @param: @param user
	 * @param: @return      
	 * @return: ResponseVO      
	 * @throws
	 */
	@ResponseBody
	@RequestMapping("/removeUser")
	public ResponseVO removeUser(@RequestBody List<User> userList){
		ResponseVO responseVO=new ResponseVO();
		String result=userServiceApi.removeUser(userList);
		responseVO.setReturnCode(result);
		return responseVO;
	}
	
	

}