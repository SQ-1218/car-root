package com.magicrule.car.systemManage.configClass.spring;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * 
 * @ClassName:  DateConverterConfig   
 * @Description:TODO(前端传递过来的日期字符串转换成日期类型对象)   
 * @author: hebb
 * @date:   2018年12月31日 下午5:35:09   
 *     
 * @Copyright: 2018 www.magicruler.com Inc. All rights reserved. 
 * 注意：本内容仅限于魔尺信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目的
 */
@Component
public class DateConverterConfig implements Converter<String,Date>{
	
	private static final List<String> formats=new ArrayList<String>(4);
	
	static {
		formats.add("yyyy-MM");
		formats.add("yyyy-MM-dd");
		formats.add("yyyy-MM-dd hh:mm");
		formats.add("yyyy-MM-dd hh:mm:ss");
	}
	 
    @Override
    public Date convert(String source) {
        String value = source.trim();
        if ("".equals(value)) {
            return null;
        }
        if(source.matches("^\\d{4}-\\d{1,2}$")){
            return parseDate(source, formats.get(0));
        }else if(source.matches("^\\d{4}-\\d{1,2}-\\d{1,2}$")){
            return parseDate(source, formats.get(1));
        }else if(source.matches("^\\d{4}-\\d{1,2}-\\d{1,2} {1}\\d{1,2}:\\d{1,2}$")){
            return parseDate(source, formats.get(2));
        }else if(source.matches("^\\d{4}-\\d{1,2}-\\d{1,2} {1}\\d{1,2}:\\d{1,2}:\\d{1,2}$")){
            return parseDate(source, formats.get(3));
        }else {
            throw new IllegalArgumentException("Invalid boolean value '" + source + "'");
        }
    }


        public  Date parseDate(String source, String format) {
            Date date=null;
            try {
                DateFormat dateFormat = new SimpleDateFormat(format);
                date = dateFormat.parse(source);
            } catch (Exception e) {
            	 throw new RuntimeException(String.format("parser %s to Date fail", source));
            }
            return date;
        }

}
