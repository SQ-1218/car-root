package com.magicrule.car.systemManage.controller.resource;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.magicrule.car.systemManage.configClass.redis.RedisUtil;
import com.magicrule.car.systemManage.controller.base.BaseController;
import com.magicrule.car.systemManage.model.Resource;
import com.magicrule.car.systemManage.model.ResponseVO;
import com.magicrule.car.systemManage.service.api.ResourceServiceApi;
import com.magicrule.prj.common.CommonUtils;
import com.magicrule.prj.common.ConfigConsts;
import com.magicrule.prj.common.page.PageParam;
import com.magicrule.prj.common.pinyin.ChineseCharToPy;


/**
 * 
 * @ClassName:  ResourceController   
 * @Description:TODO(菜单资源管理)   
 * @author: hebb
 * @date:   2019年2月22日 下午10:52:45   
 *     
 * @Copyright: 2019 www.magicruler.com Inc. All rights reserved. 
 * 注意：本内容仅限于魔尺信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目的
 */
@Controller
@RequestMapping(value="/systemManage/resource")
public class ResourceController extends BaseController {

	protected static Logger log = LoggerFactory.getLogger(ResourceController.class);
	
	@Autowired
	ResourceServiceApi resourceServiceApi;
	
	@Autowired
	RedisUtil redisUtil;
	
	/**
	 * 
	 * @Title: resourceList   
	 * @Description: TODO(跳转到菜单资源列表操作页面)   
	 * @param: @param resource
	 * @param: @return      
	 * @return: String      
	 * @throws
	 */
	@RequestMapping("/resourceList")
	public String resourceList(Resource resource){
		return "systemManage/resource/resourceList";
	}
	
	/**
	 * 
	 * @Title: findResourceList   
	 * @Description: TODO(查询菜单资源列表信息)   
	 * @param: @param role
	 * @param: @param pageParam
	 * @param: @return      
	 * @return: PageInfo<Role>      
	 * @throws
	 */
	@ResponseBody
	@RequestMapping("/findResourceList")
	public PageInfo<Resource> findResourceList(Resource resource, PageParam pageParam){
		String orderBy=null;
		if(StringUtils.hasText(pageParam.getSort())) {
			orderBy=pageParam.getSort()+" "+pageParam.getOrder();
		}
		
		PageHelper.startPage(pageParam.getPage(),pageParam.getRows(),orderBy);
		List<Resource> resourceList=resourceServiceApi.findResourceList(resource);
		PageInfo<Resource> pageInfo = new PageInfo<Resource>(resourceList);
		
	    return pageInfo;
	}
	
	

	/**
	 * 
	 * @Title: getResourceById   
	 * @Description: TODO(根据id查询菜单资源)   
	 * @param: @param id
	 * @param: @return      
	 * @return: ResponseVO      
	 * @throws
	 */
	@ResponseBody
	@RequestMapping("/getResourceById")
	public ResponseVO getResourceById(Long id){
		ResponseVO responseVO=new ResponseVO();
		responseVO.setReturnCode(ConfigConsts.SUCCESS);
		
		Resource resource=resourceServiceApi.getResourceById(id);
		if(resource!=null) {
			responseVO.setReturnData(resource);
		}else {
			responseVO.setReturnCode(ConfigConsts.FAIL);
		}
		
		return responseVO;
	}
	
	

	/**
	 * 
	 * @Title: removeResource   
	 * @Description: TODO(删除菜单资源)   
	 * @param: @param resourceList
	 * @param: @return      
	 * @return: ResponseVO      
	 * @throws
	 */
	@ResponseBody
	@RequestMapping("/removeResource")
	public ResponseVO removeResource(@RequestBody List<Resource> resourceList){
		ResponseVO responseVO=new ResponseVO();
		String result=resourceServiceApi.removeResource(resourceList);
		responseVO.setReturnCode(result);
		return responseVO;
	}
	

	/**
	 * 
	 * @Title: updateResource   
	 * @Description: TODO(修改菜单资源)
	 * @param: @param resource
	 * @param: @return
	 * @param: @throws Exception      
	 * @return: ResponseVO      
	 * @throws
	 */
	@ResponseBody
	@RequestMapping("/updateResource")
	public ResponseVO updateResource(Resource resource) throws Exception{
		ResponseVO responseVO=new ResponseVO();
		responseVO.setReturnCode(ConfigConsts.SUCCESS);
		
		setModifierInfo(resource);
		
		resourceServiceApi.updateResource(resource);
		return responseVO;
		
	}
	

	/**
	 * 
	 * @Title: insertRole   
	 * @Description: TODO(新增菜单资源)   
	 * @param: @param role
	 * @param: @return
	 * @param: @throws Exception      
	 * @return: ResponseVO      
	 * @throws
	 */
	@ResponseBody
	@RequestMapping("/insertResource")
	public ResponseVO insertResource(Resource resource) throws Exception{
		
		ResponseVO responseVO=new ResponseVO();
		responseVO.setReturnCode(ConfigConsts.SUCCESS);
		
		setCreatorInfo(resource);
		
		resourceServiceApi.insertResource(resource);
	    return responseVO;
		
	}
	
	/**
	 * 
	 * @Title: generateResourceCode   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   
	 * @param: @param resource
	 * @param: @return
	 * @param: @throws Exception      
	 * @return: ResponseVO      
	 * @throws
	 */
	@ResponseBody
	@RequestMapping("/generateResourceCode")
	public ResponseVO generateResourceCode(Resource resource) throws Exception{
		
		ResponseVO responseVO=new ResponseVO();
		responseVO.setReturnCode(ConfigConsts.SUCCESS);
		
		if(StringUtils.hasText(resource.getResourceName())) {
			String allFirstLetter=ChineseCharToPy.getAllFirstLetter(resource.getResourceName());
			String resourceCode=CommonUtils.generateStationaryChar(allFirstLetter,3);
			int n=0;
			while(n<20) {
				n++;
				int count=resourceServiceApi.getResourceCountByCode(resourceCode);
				if(count>0) {
					resourceCode=CommonUtils.generateAchar(3);
				}else {
					break;
				}
			}
			responseVO.setReturnData(resourceCode);
		}else {
			responseVO.setReturnCode(ConfigConsts.FAIL);
		}
		
	    return responseVO;
		
	}
	
	
	
}
