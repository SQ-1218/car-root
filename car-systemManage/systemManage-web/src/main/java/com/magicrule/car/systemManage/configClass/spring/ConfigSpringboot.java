package com.magicrule.car.systemManage.configClass.spring;

import java.util.List;

import javax.servlet.Servlet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.boot.autoconfigure.http.HttpMessageConverters;
import org.springframework.boot.autoconfigure.web.ResourceProperties;
import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.springframework.boot.autoconfigure.web.servlet.WebMvcAutoConfiguration;
import org.springframework.boot.autoconfigure.web.servlet.error.ErrorViewResolver;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.web.servlet.DispatcherServlet;

import com.alibaba.fastjson.support.spring.FastJsonHttpMessageConverter;
import com.magicrule.car.systemManage.configClass.exceptionHandler.MyBaseErrorController;

/**
 * 
 * @ClassName:  ConfigSpringboot   
 * @Description:TODO(springboot的配置类)   
 * @author: hebb
 * @date:   2018年12月6日 下午10:00:27   
 *     
 * @Copyright: 2018 www.magicruler.com Inc. All rights reserved. 
 * 注意：本内容仅限于魔尺信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目的
 */
@Order(11)
@Configuration
@ConditionalOnWebApplication
@ConditionalOnClass({Servlet.class, DispatcherServlet.class})
@AutoConfigureBefore(WebMvcAutoConfiguration.class)
@EnableConfigurationProperties(ResourceProperties.class)
public class ConfigSpringboot {
	
    @Autowired(required = false)
    private List<ErrorViewResolver> errorViewResolvers;
    
    private final ServerProperties serverProperties;

    public ConfigSpringboot(
            ServerProperties serverProperties) {
        this.serverProperties = serverProperties;
    }

	/**
	 *  使用FastJson作为系统的转换
	 * @return
	 */
	@Bean 
	public HttpMessageConverters fastMessageConverters(){
	 return new HttpMessageConverters(new FastJsonHttpMessageConverter()); 
	}

	
    /**
     * 自定义异常处理类的Bean配置
     * @param errorAttributes
     * @return
     */
    @Bean
    public MyBaseErrorController baseErrorController(ErrorAttributes errorAttributes) {
        return new MyBaseErrorController(errorAttributes, this.serverProperties.getError(),
                this.errorViewResolvers);
    }
    

}
