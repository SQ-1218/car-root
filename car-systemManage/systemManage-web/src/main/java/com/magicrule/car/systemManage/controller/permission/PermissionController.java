package com.magicrule.car.systemManage.controller.permission;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.magicrule.car.systemManage.configClass.redis.RedisUtil;
import com.magicrule.car.systemManage.controller.base.BaseController;
import com.magicrule.car.systemManage.model.Permission;
import com.magicrule.car.systemManage.model.ResponseVO;
import com.magicrule.car.systemManage.service.api.PermissionServiceApi;
import com.magicrule.prj.common.ConfigConsts;
import com.magicrule.prj.common.page.PageParam;

/**
 * 
 * @ClassName:  PermissionController   
 * @Description:TODO(权限管理)   
 * @author: hebb
 * @date:   2019年2月23日 下午5:42:49   
 *     
 * @Copyright: 2019 www.magicruler.com Inc. All rights reserved. 
 * 注意：本内容仅限于魔尺信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目的
 */
@Controller
@RequestMapping(value="/systemManage/permission")
public class PermissionController extends BaseController {

	protected static Logger log = LoggerFactory.getLogger(PermissionController.class);
	
	@Autowired
	PermissionServiceApi permissionServiceApi;
	
	@Autowired
	RedisUtil redisUtil;
	
	/**
	 * 
	 * @Title: permissionList   
	 * @Description: TODO(跳转到权限列表操作页面)   
	 * @param: @param permission
	 * @param: @return      
	 * @return: String      
	 * @throws
	 */
	@RequestMapping("/permissionList")
	public String permissionList(Permission permission){
		return "systemManage/permission/permissionList";
	}
	
	/**
	 * 
	 * @Title: findPermissionList   
	 * @Description: TODO(查询权限列表信息)   
	 * @param: @param role
	 * @param: @param pageParam
	 * @param: @return      
	 * @return: PageInfo<Role>      
	 * @throws
	 */
	@ResponseBody
	@RequestMapping("/findPermissionList")
	public PageInfo<Permission> findPermissionList(Permission permission, PageParam pageParam){
		String orderBy=null;
		if(StringUtils.hasText(pageParam.getSort())) {
			orderBy=pageParam.getSort()+" "+pageParam.getOrder();
		}
		
		PageHelper.startPage(pageParam.getPage(),pageParam.getRows(),orderBy);
		List<Permission> permissionList=permissionServiceApi.findPermissionList(permission);
		PageInfo<Permission> pageInfo = new PageInfo<Permission>(permissionList);
		
	    return pageInfo;
	}
	
	

	/**
	 * 
	 * @Title: getPermissionById   
	 * @Description: TODO(根据id查询权限)   
	 * @param: @param id
	 * @param: @return      
	 * @return: ResponseVO      
	 * @throws
	 */
	@ResponseBody
	@RequestMapping("/getPermissionById")
	public ResponseVO getPermissionById(String id){
		ResponseVO responseVO=new ResponseVO();
		responseVO.setReturnCode(ConfigConsts.SUCCESS);
		
		Permission permission=permissionServiceApi.getPermissionById(id);
		if(permission!=null) {
			responseVO.setReturnData(permission);
		}else {
			responseVO.setReturnCode(ConfigConsts.FAIL);
		}
		
		return responseVO;
	}
	
	

	/**
	 * 
	 * @Title: removePermission   
	 * @Description: TODO(删除权限)   
	 * @param: @param permissionList
	 * @param: @return      
	 * @return: ResponseVO      
	 * @throws
	 */
	@ResponseBody
	@RequestMapping("/removePermission")
	public ResponseVO removePermission(@RequestBody List<Permission> permissionList){
		ResponseVO responseVO=new ResponseVO();
		String result=permissionServiceApi.removePermission(permissionList);
		responseVO.setReturnCode(result);
		return responseVO;
	}
	

	/**
	 * 
	 * @Title: updatePermission   
	 * @Description: TODO(修改权限)
	 * @param: @param permission
	 * @param: @return
	 * @param: @throws Exception      
	 * @return: ResponseVO      
	 * @throws
	 */
	@ResponseBody
	@RequestMapping("/updatePermission")
	public ResponseVO updatePermission(Permission permission) throws Exception{
		ResponseVO responseVO=new ResponseVO();
		responseVO.setReturnCode(ConfigConsts.SUCCESS);
		
		setModifierInfo(permission);
		
		permissionServiceApi.updatePermission(permission);
		return responseVO;
		
	}
	

	/**
	 * 
	 * @Title: insertRole   
	 * @Description: TODO(新增权限)   
	 * @param: @param role
	 * @param: @return
	 * @param: @throws Exception      
	 * @return: ResponseVO      
	 * @throws
	 */
	@ResponseBody
	@RequestMapping("/insertPermission")
	public ResponseVO insertPermission(Permission permission) throws Exception{
		
		ResponseVO responseVO=new ResponseVO();
		responseVO.setReturnCode(ConfigConsts.SUCCESS);
		
		setCreatorInfo(permission);
		
		permissionServiceApi.insertPermission(permission);
	    return responseVO;
		
	}
	

}
