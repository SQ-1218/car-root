package com.magicrule.car.systemManage.configClass.spring;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * 
 * @ClassName:  ApplicationContextUtil   
 * @Description:TODO(获取Bean)   
 * @author: hebb
 * @date:   2018年12月14日 下午9:46:47   
 *     
 * @Copyright: 2018 www.magicruler.com Inc. All rights reserved. 
 * 注意：本内容仅限于魔尺信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目的
 */
public class ApplicationContextUtil implements ApplicationContextAware{
	
	protected static Logger log = LoggerFactory.getLogger(ApplicationContextUtil.class);  
	
	
	private static ApplicationContext applicationContext = null;
	 
	@Override
	public  void setApplicationContext(ApplicationContext applicationContext) throws BeansException {

		if(ApplicationContextUtil.applicationContext == null){
			ApplicationContextUtil.applicationContext  = applicationContext;
			log.info("---------------------------------------------------------------------");
			log.info("========ApplicationContext配置成功,在普通类可以通过调用ApplicationContextUtil.getAppContext()获取applicationContext对象,applicationContext="+applicationContext+"========");
			log.info("---------------------------------------------------------------------");
		}
	}
	 
	    //获取applicationContext
	    public static ApplicationContext getApplicationContext() {
	        return applicationContext;
	    }

	    //通过name获取 Bean.
	    public static Object getBean(String name){
	        return getApplicationContext().getBean(name);
	    }

	    //通过class获取Bean.
	    public static <T> T getBean(Class<T> clazz){
	        return getApplicationContext().getBean(clazz);
	    }

	    //通过name,以及Clazz返回指定的Bean
	    public static <T> T getBean(String name,Class<T> clazz){
	        return getApplicationContext().getBean(name, clazz);
	    }

	}
