package com.magicrule.car.systemManage.controller.base;



import java.util.Date;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;

import com.magicrule.car.systemManage.configClass.shiro.PasswordHelper;
import com.magicrule.car.systemManage.model.User;
/**
 * 
 * @ClassName:  BaseController   
 * @Description:TODO(抽象Contoller父类)   
 * @author: hebb
 * @date:   2019年1月17日 下午10:34:54   
 *     
 * @Copyright: 2019 www.magicruler.com Inc. All rights reserved. 
 * 注意：本内容仅限于魔尺信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目的
 */
public abstract class BaseController {

	/**
	 * 
	 * @Title: getCurrentUser   
	 * @Description: TODO(获取当前登录的用户)   
	 * @param: @return      
	 * @return: User      
	 * @throws
	 */
	public static User getCurrentUser() {
		Subject subject = SecurityUtils.getSubject();  
        if(null != subject){  
            Session session = subject.getSession();  
            if(null != session){  
            	return (User)session.getAttribute("currentUser"); 
            }  
        }
        return null;
	}
	
	/**
	 * 
	 * @Title: encryptPassword   
	 * @Description: TODO(用户密码加密)   
	 * @param: @param user
	 * @param: @return      
	 * @return: User      
	 * @throws
	 */
	public static User encryptPassword(User user) {
		PasswordHelper phelper=new PasswordHelper();
		user=phelper.encryptPassword(user);
		return user;
	}
	
	/**
	 * 
	 * @Title: setCreatorInfo   
	 * @Description: TODO(设置创建人信息)   
	 * @param: @param user      
	 * @return: void      
	 * @throws
	 */
	public static void setCreatorInfo(Object obj) throws Exception {
		User currentUser=getCurrentUser();    
		if(currentUser!=null) {
			BeanUtils.setProperty(obj, "creatorId", currentUser.getId());
			BeanUtils.setProperty(obj, "creatorName", currentUser.getUserName());
			BeanUtils.setProperty(obj, "creatorTrueName", currentUser.getTrueName());
		}
		BeanUtils.setProperty(obj, "createDate", new Date());
	}
	
	/**
	 * 
	 * @Title: setModifierInfo   
	 * @Description: TODO(设置修改人信息)   
	 * @param: @param user      
	 * @return: void      
	 * @throws
	 */
	public static void setModifierInfo(Object obj) throws Exception {
		User currentUser=getCurrentUser();    
		if(currentUser!=null) {
			BeanUtils.setProperty(obj, "modifierId", currentUser.getId());
			BeanUtils.setProperty(obj, "modifierName", currentUser.getUserName());
			BeanUtils.setProperty(obj, "modifierTrueName", currentUser.getTrueName());
		}
		BeanUtils.setProperty(obj, "modifyDate", new Date());
	}
	
	
}
