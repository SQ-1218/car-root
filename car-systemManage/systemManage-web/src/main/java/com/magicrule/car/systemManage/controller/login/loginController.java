package com.magicrule.car.systemManage.controller.login;

import javax.servlet.http.HttpSession;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.ExcessiveAttemptsException;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.magicrule.car.systemManage.model.ResponseVO;
import com.magicrule.car.systemManage.model.User;
import com.magicrule.prj.common.ConfigConsts;

/**
 * 
 * @ClassName: loginController
 * @Description:TODO(登录)
 * @author: hebb
 * @date: 2018年12月7日 下午9:59:18
 * 
 * @Copyright: 2018 www.magicruler.com Inc. All rights reserved.
 *             注意：本内容仅限于魔尺信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目的
 */
@Controller
public class loginController {

	protected static Logger log = LoggerFactory.getLogger(loginController.class);  
	

	/**
	 * 
	 * @Title: defaultIndex   
	 * @Description: TODO(默认主页)   
	 * @param: @param model
	 * @param: @return      
	 * @return: String      
	 * @throws
	 */
	@RequestMapping("/")
	public String defaultIndex(Model model) {
	    return "forward:index";
	}

	/**
	 * 
	 * @Title: login 
	 * @Description: TODO(跳转到登录页面) 
	 * String @throws
	 */
	@RequestMapping("/login*")
	public String login() {
		return "homePage/login";
	}
	
	
	/**
	 * 
	 * @Title: login 
	 * @Description: TODO(跳转到首页) 
	 * String @throws
	 */
	@RequestMapping("/index*")
	public String index() {
		return "index.html";
	}
	
	
	/**
	 * 
	 * @Title: loginUser   
	 * @Description: TODO(用户登录验证)   
	 * @param: @param username
	 * @param: @param password
	 * @param: @param session
	 * @param: @param model
	 * @param: @param map
	 * @param: @return      
	 * @return: String      
	 * @throws
	 */
	@RequestMapping("/loginUser")
	@ResponseBody
	public ResponseVO loginUser(User user, HttpSession session, Model model, ModelMap map) {
		ResponseVO responseVO=new ResponseVO();
		responseVO.setReturnCode(ConfigConsts.SUCCESS);
		UsernamePasswordToken usernamePasswordToken = new UsernamePasswordToken(user.getUserName(), user.getPassword());
		Subject subject = SecurityUtils.getSubject();
		try {
			subject.login(usernamePasswordToken);// 完成登录
			model.addAttribute("user", session.getAttribute("currentUser"));
		} catch (IncorrectCredentialsException ice) {
			// 捕获密码错误异常
			responseVO.setReturnCode(ConfigConsts.FAIL);
			responseVO.setReturnMessage("用户名或者密码错误");
		} catch (UnknownAccountException uae) {
			// 捕获未知用户名异常
			responseVO.setReturnCode(ConfigConsts.FAIL);
			responseVO.setReturnMessage("用户名或者密码错误");
		} catch (ExcessiveAttemptsException eae) {
			// 捕获错误登录过多的异常
			responseVO.setReturnCode(ConfigConsts.FAIL);
			responseVO.setReturnMessage("登录次数过错，请2分钟后再尝试");
		} catch (Exception e) {
			// 返回登录页面
			responseVO.setReturnCode(ConfigConsts.FAIL);
			responseVO.setReturnMessage("登录出错，请稍后在尝试");
		}
		
		return responseVO;

	}

	/**
	 * 
	 * @Title: logout   
	 * @Description: TODO(注销)   
	 * @param: @param session
	 * @param: @return      
	 * @return: String      
	 * @throws
	 */
	@RequestMapping("/logout")
	public String logout(HttpSession session) {
		Subject subject = SecurityUtils.getSubject();
		subject.logout();
		return "homePage/login";
	}

}
