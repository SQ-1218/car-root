package com.magicrule.car.systemManage.model;

import java.io.Serializable;
import java.util.Date;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * 
 * @ClassName:  BaseEntity   
 * @Description:TODO(父实体类)   
 * @author: hebb
 * @date:   2018年11月28日 下午9:26:35   
 *     
 * @Copyright: 2018 www.magicruler.com Inc. All rights reserved. 
 * 注意：本内容仅限于魔尺信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目的
 */
public abstract class BaseEntity  implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * 是否可用(Y:启用;N:禁用),长度2
	 */
	private String enabled;
	

	/**
	 * 创建人Id
	 */
	private Long creatorId;
	
	/**
	 * 修改人Id
	 */
	private Long modifierId;
	
	/**
	 * 创建人用户名
	 */
	private String creatorName;
	
	/**
	 * 修改人用户名
	 */
	private String modifierName;
	
	/**
	 * 创建日期
	 */
	@JSONField (format="yyyy-MM-dd HH:mm:ss")
	private Date createDate;

	/**
	 * 修改日期
	 */
	@JSONField (format="yyyy-MM-dd HH:mm:ss")
	private Date modifyDate;
	
	/**
	 * 创建人姓名
	 */
	private String creatorTrueName;
	
	/**
	 * 修改人姓名
	 */
	private String modifierTrueName;
	
	
	public Long getCreatorId() {
		return creatorId;
	}

	public void setCreatorId(Long creatorId) {
		this.creatorId = creatorId;
	}

	public Long getModifierId() {
		return modifierId;
	}

	public void setModifierId(Long modifierId) {
		this.modifierId = modifierId;
	}

	public String getCreatorName() {
		return creatorName;
	}

	public void setCreatorName(String creatorName) {
		this.creatorName = creatorName;
	}

	public String getModifierName() {
		return modifierName;
	}

	public void setModifierName(String modifierName) {
		this.modifierName = modifierName;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public String getCreatorTrueName() {
		return creatorTrueName;
	}

	public void setCreatorTrueName(String creatorTrueName) {
		this.creatorTrueName = creatorTrueName;
	}

	public String getModifierTrueName() {
		return modifierTrueName;
	}

	public void setModifierTrueName(String modifierTrueName) {
		this.modifierTrueName = modifierTrueName;
	}

	public String getEnabled() {
		return enabled;
	}

	public void setEnabled(String enabled) {
		this.enabled = enabled;
	}

	
	
	
	
}
