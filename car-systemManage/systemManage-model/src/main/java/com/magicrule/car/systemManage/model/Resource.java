package com.magicrule.car.systemManage.model;

import org.springframework.data.annotation.Transient;

/**
 * 
 * @ClassName:  Resource   
 * @Description:TODO(资源，例如：菜单)   
 * @author: hebb
 * @date:   2018年12月8日 下午10:03:32   
 *     
 * @Copyright: 2018 www.magicruler.com Inc. All rights reserved. 
 * 注意：本内容仅限于魔尺信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目的
 */
public class Resource extends BaseEntity {

	private static final long serialVersionUID = 1L;

	/**
	 * 主键，长度20
	 */
	private Long id;
	
	/**
	 * 资源名称,长度50
	 */
	private String resourceName;
	
	/**
	 * 资源编码,长度50
	 */
	private String code;
	
	
	/**
	 * 资源url,长度100
	 */
	private String url;
	
	/**
	 * 序号,长度5
	 */
	private Integer sort;
	
	/**
	 * 父id
	 */
	private Long parentId;
	
	/**
	 * 父名称,长度50
	 */
	private String parentName;
	
	/**
	 * 层级,长度2
	 */
	private Integer level;
	
	/**
	 * 起始时间
	 */
	@Transient
	private String startDate;
	
	/**
	 * 结束时间
	 */
	@Transient
	private String endDate;
	
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getResourceName() {
		return resourceName;
	}

	public void setResourceName(String resourceName) {
		this.resourceName = resourceName;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public String getParentName() {
		return parentName;
	}

	public void setParentName(String parentName) {
		this.parentName = parentName;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}
	
	
	
}
