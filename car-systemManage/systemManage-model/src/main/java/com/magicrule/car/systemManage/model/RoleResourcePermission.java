package com.magicrule.car.systemManage.model;

import java.io.Serializable;

/**
 * 
 * @ClassName:  RoleResourcePermission   
 * @Description:TODO(角色-资源-权限关联)   
 * @author: hebb
 * @date:   2018年12月8日 下午10:01:06   
 *     
 * @Copyright: 2018 www.magicruler.com Inc. All rights reserved. 
 * 注意：本内容仅限于魔尺信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目的
 */
public class RoleResourcePermission implements Serializable{

	private static final long serialVersionUID = 1L;

	/**
	 * 角色名称
	 */
	private String roleName;
	
	/**
	 * 资源名称
	 */
	private String resourceName;
	
	/**
	 * 权限名称
	 */
	private String permissionName;

	/**
	 * shiro的资源:权限匹配格式
	 * resourcePermission=resourceName:permiessionName
	 */
	private String resourcePermission;

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getResourceName() {
		return resourceName;
	}

	public void setResourceName(String resourceName) {
		this.resourceName = resourceName;
	}

	public String getPermissionName() {
		return permissionName;
	}

	public void setPermissionName(String permissionName) {
		this.permissionName = permissionName;
	}

	public String getResourcePermission() {
		return resourcePermission;
	}

	public void setResourcePermission(String resourcePermission) {
		this.resourcePermission = resourcePermission;
	}
	
	
}
