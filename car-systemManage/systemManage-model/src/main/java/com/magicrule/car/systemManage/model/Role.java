package com.magicrule.car.systemManage.model;

import org.springframework.data.annotation.Transient;

/**
 * 
 * @ClassName:  Role   
 * @Description:TODO(角色)   
 * @author: hebb
 * @date:   2018年12月8日 下午9:57:43   
 *     
 * @Copyright: 2018 www.magicruler.com Inc. All rights reserved. 
 * 注意：本内容仅限于魔尺信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目的
 */
public class Role extends BaseEntity {
	
	private static final long serialVersionUID = 1L;

	private Long id;
	
	/**
	 * 角色名称
	 */
	private String roleName;
	
	/**
	 * 起始时间
	 */
	@Transient
	private String startDate;
	
	/**
	 * 结束时间
	 */
	@Transient
	private String endDate;
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	
	
}
