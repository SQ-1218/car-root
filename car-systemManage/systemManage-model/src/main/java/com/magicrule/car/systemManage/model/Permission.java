package com.magicrule.car.systemManage.model;

import org.springframework.data.annotation.Transient;

/**
 * 
 * @ClassName:  Permission   
 * @Description:TODO(权限)   
 * @author: hebb
 * @date:   2018年12月8日 下午9:59:26   
 *     
 * @Copyright: 2018 www.magicruler.com Inc. All rights reserved. 
 * 注意：本内容仅限于魔尺信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目的
 */
public class Permission extends BaseEntity {

	private static final long serialVersionUID = 1L;

	private Long id;
	
	/**
	 * 权限名称
	 */
	private String permissionName;
	
	/**
	 * 权限编码
	 */
	private String code;
	
	/**
	 * 起始时间
	 */
	@Transient
	private String startDate;
	
	/**
	 * 结束时间
	 */
	@Transient
	private String endDate;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPermissionName() {
		return permissionName;
	}

	public void setPermissionName(String permissionName) {
		this.permissionName = permissionName;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
	
}
