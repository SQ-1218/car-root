package com.magicrule.car.systemManage.model;

import java.io.Serializable;

/**
 * 
 * @ClassName:  ResponseVO   
 * @Description:TODO(Controller的统一返回)   
 * @author: hebb
 * @date:   2018年12月8日 下午9:41:02   
 *     
 * @Copyright: 2018 www.magicruler.com Inc. All rights reserved. 
 * 注意：本内容仅限于魔尺信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目的
 */
public class ResponseVO implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 返回的状态码
	 */
	private String returnCode;
	
	/**
	 * 返回的信息
	 */
	private String returnMessage;
	
	/**
	 * 返回的数据
	 */
	private Object returnData;

	public String getReturnCode() {
		return returnCode;
	}

	public void setReturnCode(String returnCode) {
		this.returnCode = returnCode;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	public Object getReturnData() {
		return returnData;
	}

	public void setReturnData(Object returnData) {
		this.returnData = returnData;
	}
	
	
	
}
