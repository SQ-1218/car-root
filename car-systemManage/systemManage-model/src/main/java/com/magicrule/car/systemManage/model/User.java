package com.magicrule.car.systemManage.model;

import org.springframework.data.annotation.Transient;

/**
 * 
 * @ClassName:  UserVO   
 * @Description:TODO(用户信息)   
 * @author: hebb
 * @date:   2018年11月19日 下午9:57:49   
 *     
 * @Copyright: 2018 www.magicruler.com Inc. All rights reserved. 
 * 注意：本内容仅限于魔尺信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目的
 */
public class User extends BaseEntity {
	
	private static final long serialVersionUID = 1L;

	private Long id;
	
	/**
	 * 登录名称
	 */
	private String userName;
	
	/**
	 * 密码
	 */
	private String password;
	
	/**
	 * 真实姓名
	 */
	private String trueName;
	
	private String salt;
	
	/**
	 * credentialsSalt=userName+salt
	 */
	private String credentialsSalt;
	
	
	/**
	 * 起始时间
	 */
	@Transient
	private String startDate;
	
	/**
	 * 结束时间
	 */
	@Transient
	private String endDate;



	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getSalt() {
		return salt;
	}

	public void setSalt(String salt) {
		this.salt = salt;
		this.credentialsSalt=userName+salt;
	}

	public String getCredentialsSalt() {
		return credentialsSalt;
	}

	public void setCredentialsSalt(String credentialsSalt) {
		this.credentialsSalt = credentialsSalt;
	}

	public String getTrueName() {
		return trueName;
	}

	public void setTrueName(String trueName) {
		this.trueName = trueName;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	
	
	
	
}
