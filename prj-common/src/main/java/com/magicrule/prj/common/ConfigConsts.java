package com.magicrule.prj.common;

/**
 * 
 * @ClassName:  Constants   
 * @Description:TODO(常量定义)   
 * @author: hebb
 * @date:   2018年12月8日 下午9:48:10   
 *     
 * @Copyright: 2018 www.magicruler.com Inc. All rights reserved. 
 * 注意：本内容仅限于魔尺信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目的
 */
public class ConfigConsts {

	/**
	 * 成功的状态码
	 */
	public final static  String SUCCESS="success";
	
	/**
	 * 失败的状态的码
	 */
	public final static String FAIL="fail";
	
	/**
	 * 存在可用状态的数据
	 */
	public final static  String EXIST_ENABLED="exist_enabled";
	
	
}
