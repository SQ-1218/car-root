package com.magicrule.prj.common.page;

import java.io.Serializable;

/**
 * 
 * @ClassName:  PageParam   
 * @Description:TODO(接受easyui传递过来的分页参数)   
 * @author: hebb
 * @date:   2019年1月3日 下午10:09:34   
 *     
 * @Copyright: 2019 www.magicruler.com Inc. All rights reserved. 
 * 注意：本内容仅限于魔尺信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目的
 */
public class PageParam implements Serializable{
	
	private static final long serialVersionUID = 1L;

	/**
	 * 当前页数
	 */
	int page;
	
	/**
	 * 一页显示的个数
	 */
	int rows;
  
	String sort;
	
	String order;

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getRows() {
		return rows;
	}

	public void setRows(int rows) {
		this.rows = rows;
	}

	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}

	public String getOrder() {
		return order;
	}

	public void setOrder(String order) {
		this.order = order;
	}
	
	
}
